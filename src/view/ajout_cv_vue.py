from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
from dao.candidature_dao import CandidatureDAO
from business_object.candidature import Candidature
from dao.utilisateur_dao import UtilisateurDAO

class AjoutCvVue(VueAbstraite):
    def __init__(self, message = ""):
        super().__init__(message)
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Voulez vous associer une lettre de motivation ou un cv à une annonce ?",
                "choices": [
                    "Oui",
                    "Non",
                ],
            },
            {"type": "input", 
            "name": "id_annonce", 
            "when": lambda answers: answers['choix'] == "Oui",
            "message": "Quel est l'identifiant de l'annonce à laquelle vous voulez associer un document ?"},
            {"type": "input", 
            "name": "lettre", 
            "when": lambda answers: answers['choix'] == "Oui",
            "message": "Quel est le chemin d'accès de votre lettre de motivation ?"},
            {"type": "input", 
            "name": "cv", 
            "when": lambda answers: answers['choix'] == "Oui",
            "message": "Quel est le chemin d'accès de votre CV ?"}
        ]

    def choisir_menu(self, pseudo):
        from view.voir_profil_vue import VoirProfilVue
        reponse = prompt(self.questions)
        choix = reponse["choix"]

        if choix == "Oui":
            id_annonce = reponse["id_annonce"]
            cv = reponse["cv"]
            lettre = reponse["lettre"]
            id_utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo).id_utilisateur
            candidature = Candidature(id_utilisateur, id_annonce, cv, lettre)
            if CandidatureDAO().miseajour_candidature(candidature):
                print("Vos documents ont bien été ajoutés !")
            else:
                print("L'identifiant de l'annonce ne correspond pas à une annonce enregistrée.")
        return VoirProfilVue().choisir_menu(pseudo)
            