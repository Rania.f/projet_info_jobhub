from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
from service.utilisateur_connecte import UtilisateurConnecte


class ModifMdpVue(VueAbstraite):
    def __init__(self):
        message = "Modifier son mot de passe."
        super().__init__(message)
        self.questions = [
            {"type": "input", "name": "pseudo", "message": "Entrez votre pseudonyme :"},
            {
                "type": "password",
                "name": "mdp",
                "message": "Entrez votre mot de passe actuel :",
            },
            {
                "type": "password",
                "name": "new_mdp",
                "message": "Entrez votre nouveau mot de passe :",
            },
        ]

    def choisir_menu(self, pseudo):
        from view.menu_utilisateur_vue import MenuUtilisateurVue
        from dao.utilisateur_dao import UtilisateurDAO
        answers = prompt(self.questions)
        pseudo_verif = answers["pseudo"]
        mdp = answers["mdp"]
        new_mdp = answers["new_mdp"]
        utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo)

        message = UtilisateurConnecte(
                utilisateur.pseudo,
                utilisateur.mdp_hashe,
                utilisateur.nom,
                utilisateur.preferences,
                utilisateur.adresse,
                utilisateur.prenom,
                utilisateur.mail,
                utilisateur.tel,
            ).modifier_mdp(pseudo=pseudo_verif, mdp=mdp, new_mdp=new_mdp)

        print(message) 
        return(
            MenuUtilisateurVue().choisir_menu(pseudo)
        )


if __name__ == "__main__":
    vue = ModifMdpVue()
    vue.choisir_menu()
