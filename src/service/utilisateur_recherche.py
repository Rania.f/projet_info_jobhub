
import requests
from business_object.utilisateur import Utilisateur
from business_object.resultat_recherche import ResultatRecherche
from business_object.recherche import Recherche
from business_object.annonce import Annonce
from dao.recherche_dao import RechercheDAO
from dao.resultat_recherche_dao import ResultatRechercheDAO
from business_object.pays_enum import PaysEnum
from dao.resultat_recherche_dao import ResultatRechercheDAO
from colorama import Fore, Style


class RechercheService():
    
    def recherche_emploi(self, recherche):
        """
        Parameters
        ----------
        recherche(Recherche)
            recherche réalisée 
        Returns
        -------
        resultat_recherche(ResultatRecherche)
            Resultat de la recherche 

        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        country=recherche.country
        page= recherche.page
            
        url = f"https://api.adzuna.com/v1/api/jobs/{country}/search/{page}"
        other_params = {key: value for key, value in recherche.params.items() if key not in ["country", "page"]}
        response = requests.get(url, other_params)
# Vérifiez si la requête a réussi (code d'état 200 signifie succès)

        if response.status_code == 200:
            data = response.json()
            # créer notre objet ResultatRecherche
            nombreAnnonces = data["count"]
            results = data["results"]
            salaireMoyen = data.get("mean", None)
            listeAnnonces = []
            for dict in results:
                    listeAnnonces.append(Annonce(dict))
            return(ResultatRecherche(recherche, nombreAnnonces, salaireMoyen, listeAnnonces))

        else:
            raise ValueError(
                "La requête a échoué. Code d'état :", response.status_code)

    def filtrer_recherche(self, recherche, filtres) -> ResultatRecherche:
        """
        Parameters
        ----------
        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer 
        filtres(list):
            liste de filtres
        Returns
        -------
        resultat_recherche(ResultatRecherche)
            Resultat de la recherche 
        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        if not type(filtres) is list:
            raise TypeError("filtres doit être une liste")
        for filtre in filtres:
            if not type(filtre) is str:
                raise TypeError("filtres doit être une liste de str")

        params = recherche.params
        if "what" not in params:
            # si la clef "what" n'existe pas on la crée et on l'initialise
            params["what"] = " "

        for filtre in filtres:
            # on ajoute un espace entre les filtres
            params["what"] += " "
            params["what"] += filtre
        return (RechercheService().recherche_emploi(recherche))

    def filtrer_lieu_recherche(self, recherche, filtres_lieu) -> ResultatRecherche:
        """Définir un centre géographique pour la recherche.
        Des noms de lieux, des codes postaux, etc. 

        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        filtres_lieu(list)
            liste de filtres de positions géographiques

        Returns
        -------
        resultat_filtrage(ResultatRecherche)
            Resultat de la recherche avec filtrage

        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        if not type(filtres_lieu) is list:
            raise TypeError("filtres doit être une liste")
        for filtre in filtres_lieu:
            if not isinstance(filtre, (str)):
                raise TypeError("filtres doit être une liste de str")

        params = recherche.params
        # si la clef "what" n'existe pas on la crée et on l'initialise
        if "where" not in params:
            params["where"] = ""
        for filtre_lieu in filtres_lieu:
            # on ajoute un espace entre les filtres
            params["where"] += " "
            params["where"] += filtre_lieu
        return (RechercheService().recherche_emploi(recherche))

    def filtrer_distance_recherche(self, recherche, filtre_distance):
        """Définir la distance en kilomètres à partir du centre de l'endroit décrit par le paramètre 'where'. 
        Par défaut, elle est de 5 km.

        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        filtre_distance (int): 
            liste de filtres de positions géographiques

        Returns
        -------
        resultat_filtrage(ResultatRecherche)
            Resultat de la recherche avec filtrage 

        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        if not type(filtre_distance) is int:
            raise TypeError("filtre_distance doit être un entier")
        params = recherche.params
        # Vérifier qu'il existe un centre géographique pour la recherche
        if "where" not in params:
            raise ValueError("Veuillez définir un centre géographique")
        else:
            # on ajoute un espace entre les filtres
            params["distance"] = filtre_distance
        return (RechercheService().recherche_emploi(recherche))

    def filtrer_salaire_min(self, recherche, salaire_min):
        """
        filtrer la recherche avec un salaire minimum  


        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        salaire_min (int): 
            salaire minimum

        Returns
        -------
        resultat_filtrage(ResultatRecherche)
            Resultat de la recherche avec filtrage 

        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        if not type(salaire_min) is int:
            raise TypeError("le salaire minimum doit être un entier")
        params = recherche.params
        # Si la clé n'existe pas, on l'ajoute avec le salaire minimum défini
        params["salary_min"] = salaire_min
        return (RechercheService().recherche_emploi(recherche))

    def filtrer_salaire_max(self, recherche, salaire_max):
        """
        filtrer la recherche avec un salaire maximum  

         Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        salaire_max (int): 
            salaire maximum

        Returns
        -------
        resultat_filtrage(ResultatRecherche)
            Resultat de la recherche avec filtrage 


        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        if not type(salaire_max) is int:
            raise TypeError("le salaire maximum doit être un entier")
        params = recherche.params
        # Si la clé n'existe pas, on l'ajoute avec le salaire maximal défini
        params["salary_max"] = salaire_max
        return (RechercheService().recherche_emploi(recherche))

    def filtre_job_temps_plein(self, recherche):
        """
        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Resultat de la recherche avec uniquement des jobs à temps plein
        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        recherche.params["full_time"] = 1
        return (RechercheService().recherche_emploi(recherche))

    def filtre_job_temps_partiel(self, recherche):
        """
        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Resultat de la recherche avec uniquement des jobs à temps partiel 
        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        recherche.params["part_time"] = 1
        return (RechercheService().recherche_emploi(recherche))

    def filtre_job_en_contrat(self, recherche):
        """
        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Resultat de la recherche avec uniquement des jobs en contrat

        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        recherche.params["contract"] = 1
        return (RechercheService().recherche_emploi(recherche))

    def filtre_job_permanent(self, recherche):
        """
        Parameters
        ----------
        recherche(Recherche)
            recherche à filtrer
        
        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Resultat de la recherche avec uniquement des jobs permanent


        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        recherche.params["permanent"] = 1
        return (RechercheService().recherche_emploi(recherche))

    def enregistrer_recherche(self, recherche, id_utilisateur):
        """
        Enregistrer une recherche
        Parameters
        ----------
        recherche(Recherche)
            on enregistre cette recherche 
        id_utilisateur(int)
            id de l'utilisateur qui enregistre cette recherche
        
        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Resultat de la recherche avec uniquement des jobs permanent
        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        if not type(id_recherche) is int:
            raise TypeError("l'id doit être un entier")
        if RechercheDAO().recherche_existe(recherche.nom, id_utilisateur):
            return ("le nom de recherche est déjà utilisé")
        else:
            RechercheDAO().creation_recherche(recherche, id_utilisateur)
            return ("la recherche a bien été enregistrée")

    def enregistrer_resultat_recherche(self, recherche, id_utilisateur):
        """
        Enregistrer le résultat d'une recherche
        Parameters
        ----------
        recherche(Recherche)
            on enregistre le résultat de cette recherche 
        id_utilisateur(int)
            id de l'utilisateur qui enregistre ce résultat
        

        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Resultat de la recherche avec uniquement des jobs permanent
        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        resultat_recherche = RechercheService().recherche_emploi(recherche)
        if ResultatRechercheDAO().creation_resultat_recherche(resultat_recherche):
            return ("le résultat a bien été enregistré")
        else:
            return ("problème dans l'enregistrement")

    def passer_page_suivante(self, recherche):
        """
        Parameters
        ----------
        recherche(Recherche)
            recherche pour laquelle on passe à la page suivante

        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Renvoie les résultat de la page suivante
        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        page= int(recherche.page)
        recherche.page= page+1
        return(RechercheService().recherche_emploi(recherche))
    
    def changer_pays(self,recherche,pays):
        """
        Parameters
        ----------
        recherche(Recherche)
            la recherche pour laquelle on change le pays 
        pays(PaysEnum)
            pays dans lequel on fait la recherche

        Returns
        -------
        resultat_filtrage(ResultatRecherche):
            Renvoie le résultat
        """
        if not type(recherche) is Recherche:
            raise TypeError("recherche doit être d'instance Recherche")
        #if not type(pays) is PaysEnum:
            #raise TypeError("le pays doit être de type ¨PaysEnum")
        
        recherche.country= pays
        return(RechercheService().recherche_emploi(recherche))
        


if __name__ == '__main__':
    from dao.utilisateur_dao import UtilisateurDAO
    from utils.reset_database import ResetDatabase
    #ResetDatabase().lancer()

    utilisateur1 = Utilisateur(
        pseudo="johndoe",
        mdp_hashe="motdepasse123",
        nom="Doe",
        prenom="John",
        preferences={"langue": "français", "thème": "clair"},
        adresse="123 Rue de la Ville",
        mail="toto@gmail.com",
        tel=" 06 18 54 48 34",
    )

    recherche1 = Recherche(
        nom="",
        country=PaysEnum.FR,
        page=1,
        params={"app_id": "4aae40b4", "app_key": "2126d1c6c03226a595566bb3d7579b7a",
                "what": "banque", "results_per_page": "1"}
    )

    # # on crée l'utilisateur dans la table
    #UtilisateurDAO().creation_utilisateur(utilisateur1)
    # # on retrouve l'identifiant de l'utilisateur dans la table
    # utilisateur_dao = UtilisateurDAO().trouver_utilisateur_par_pseudo(utilisateur1.pseudo)
    # # on crée la recherche dans la table avec recherche et l'identifiant de l'utilisateur
    # RechercheDAO().creation_recherche(recherche1, utilisateur_dao.id_utilisateur)
    # # on trouve la recherche dans la table et on obtient un objet de type UtilisateurRecherche
    # recherche = RechercheDAO().trouver_recherche_par_nom_recherche_et_id_utilisateur(
    #     recherche1.nom, utilisateur_dao.id_utilisateur)
    # # on crée le résultat de la recherche
    # resultat_recherche = RechercheService().recherche_emploi(recherche)
    # # on crée le résultat de la recherche dans la table
    # ResultatRechercheDAO().creation_resultat_recherche(resultat_recherche)
    # # on cherche le résultat de la recherche dans notre table
    # for annonce in resultat_recherche.listeAnnonces:
    #     print(annonce)
    # print(recherche.id_recherche)
    # ResultatRechercheDAO().trouver_resultat_recherche_par_id_recherche(
    #     recherche.id_recherche)
    # # on enregistre la recherche et le résultat de la recherche dans la table
    # print(RechercheService().enregistrer_recherche(
    #     recherche, utilisateur_dao.id_utilisateur))
    # print(RechercheService().enregistrer_resultat_recherche(
    #     recherche, utilisateur_dao.id_utilisateur))

    #print(RechercheService().recherche_emploi(recherche1))

    # print(RechercheService().filtrer_recherche(recherche1,["Rennes"]))
    # print(RechercheService().filtrer_lieu_recherche(recherche1,["69000"]))
    # print(RechercheService().filtrer_salaire_min(recherche1,3000))
    # print(RechercheService().filtrer_salaire_max(recherche1,1000))
    # print(RechercheService().filtre_job_permanent(recherche1))
    # print(RechercheService().filtre_job_en_contrat(recherche1))
    # print(RechercheService().filtre_job_temps_plein(recherche1))
    # print(RechercheService().filtre_job_temps_partiel(recherche1))

    #print(RechercheService().passer_page_suivante(recherche1))

    #print(recherche1.params["country"])
    
    #print(RechercheService().changer_pays(recherche1,PaysEnum.GB))