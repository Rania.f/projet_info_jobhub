from abc import ABC, abstractmethod


class VueAbstraite(ABC):
    def __init__(self, message=""):
        self.message = message

    def nettoyer_console(self):
        for i in range(30):
            print("")

    @abstractmethod
    def choisir_menu(self):
        pass
