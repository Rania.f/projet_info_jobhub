import unittest
from utils.reset_database import ResetDatabase
from dao.resultat_recherche_dao import ResultatRechercheDAO
from dao.recherche_dao import RechercheDAO
from dao.annonce_dao import AnnonceDAO
from dao.db_connection import DBConnection
from business_object.resultat_recherche import ResultatRecherche
from business_object.annonce import Annonce
from business_object.recherche import Recherche
from business_object.pays_enum import PaysEnum
import json


class ResultatRechercheDaoTest(unittest.TestCase):
    def setUp(cls):
        DBConnection.CONNECTION_TO_TEST_DB = True
        ResetDatabase().lancer()

    def tearDown(cls):
        DBConnection.CONNECTION_TO_TEST_DB = False

    def test_creation_resultat_recherche(self):
        # GIVEN
        recherche_lara = Recherche("boulot lyon", PaysEnum.FR, 1, params={})
        with open("data/lara.json", "r") as lara_json:
            data = json.load(lara_json)
            # créer notre objet ResultatRecherche
            nombreAnnonces = data["count"]
            results = data["results"]
            salaireMoyen = data.get("mean", None)
            listeAnnonces = []
            for dict in results:
                listeAnnonces.append(Annonce(dict))
            resultat_recherche_lara = ResultatRecherche(
                recherche_lara, nombreAnnonces, salaireMoyen, listeAnnonces
            )

        # WHEN
        RechercheDAO().creation_recherche(recherche_lara, 2)
        success = ResultatRechercheDAO().creation_resultat_recherche(
            resultat_recherche_lara
        )
        # THEN
        # On vérifie que le résultat est enregistré :
        self.assertTrue(success)
        # On vérifie que les annonces ont étées persistées :
        for annonce in resultat_recherche_lara.listeAnnonces:
            success_annonce = AnnonceDAO().annonce_existe(annonce)
            self.assertTrue(success_annonce)

    def test_trouver_resultat_recherche_par_id_recherche(self):
        # WHEN
        # GIVEN
        resultat_recherche1 = (
            ResultatRechercheDAO().trouver_resultat_recherche_par_id_recherche(1)
        )
        resultat_recherche2 = (
            ResultatRechercheDAO().trouver_resultat_recherche_par_id_recherche(2)
        )

        # THEN
        self.assertIsNotNone(resultat_recherche1)
        self.assertIsNone(resultat_recherche2)


if __name__ == "__main__":
    unittest.main()
