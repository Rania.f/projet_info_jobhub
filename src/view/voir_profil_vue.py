from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
from service.utilisateur_service import UtilisateurService
import emoji

class VoirProfilVue(VueAbstraite):
    def __init__(self, message="") -> None:
        super().__init__(message)
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Faites votre choix",
                "choices": [
                    "\u21A9\uFE0F Revenir à la page précédante",
                    "\U0001f3e0 Modifier son profil",
                    "\U0001f512 Se déconnecter",
                    "\U0001f4c3 Voir les annonces enregistrées",
                ],
            }
        ]

    def choisir_menu(self, pseudo):
        """Choix du menu suivant de l'utilisateur

        Return
        ------
        vue
            Retourne la vue choisie par l'utilisateur dans le terminal
        """
        reponse = prompt(self.questions)

        if reponse["choix"] == "\U0001f512 Se déconnecter":
            from view.accueil_vue import AccueilVue
            return AccueilVue().choisir_menu()
        elif reponse["choix"] == "\u21A9\uFE0F Revenir à la page précédante":
            from view.menu_utilisateur_vue import MenuUtilisateurVue
            return MenuUtilisateurVue().choisir_menu(pseudo)
        elif reponse["choix"] == "\U0001f3e0 Modifier son profil":
            from view.modifier_profil_vue import ModifierProfilVue
            return ModifierProfilVue().choisir_menu(pseudo)
        elif reponse["choix"] == "\U0001f4c3 Voir les annonces enregistrées":
            from dao.utilisateur_dao import UtilisateurDAO
            from service.utilisateur_connecte import UtilisateurConnecte
            utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo)
            print(UtilisateurConnecte(
                pseudo = utilisateur.pseudo, 
                mdp_hashe=utilisateur.mdp_hashe,
                nom= utilisateur.nom,
                prenom= utilisateur.prenom,
                mail = utilisateur.mail,
                tel = utilisateur.tel,
                adresse = utilisateur.adresse,
                preferences= utilisateur.preferences).voir_annonces_enregistrees())
            if (UtilisateurConnecte(
                pseudo = utilisateur.pseudo, 
                mdp_hashe=utilisateur.mdp_hashe,
                nom= utilisateur.nom,
                prenom= utilisateur.prenom,
                mail = utilisateur.mail,
                tel = utilisateur.tel,
                adresse = utilisateur.adresse,
                preferences= utilisateur.preferences).voir_annonces_enregistrees() != "Vous n'avez pas enregistré d'annonce !"):
                from view.ajout_cv_vue import AjoutCvVue
                return(AjoutCvVue().choisir_menu(pseudo))
            else:
                from view.menu_utilisateur_vue import MenuUtilisateurVue
                return(MenuUtilisateurVue().choisir_menu(pseudo))



if __name__ == "__main__":
    vue = MenuUtilisateurVue()
    vue.choisir_menu()