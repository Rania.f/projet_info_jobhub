from business_object.utilisateur import Utilisateur
from business_object.annonce import Annonce
from business_object.recherche import Recherche
from business_object.pays_enum import PaysEnum
from business_object.candidature import Candidature
from business_object.resultat_recherche import ResultatRecherche
import unittest


class Test(unittest.TestCase):
    def test_utilisateur_attributes(self):
        # GIVEN
        user = Utilisateur(
            # id_utilisateur="123",
            nom="Curtet",
            mdp_hashe="monsupermotdepasse",
            preferences={"langue": "français"},
            # infopersos={"âge": 22, "ville": "Rennes"},
            # civilite="Monsieur",
            prenom="Dylan",
            pseudo="curtetdylan",
        )

        # WHEN - Pas d'action spécifique visible pour ce test

        # THEN
        self.assertEqual(0, user.id_utilisateur)
        self.assertEqual("Curtet", user.nom)
        self.assertEqual({"langue": "français"}, user.preferences)
        # self.assertEqual({"âge": 22, "ville": "Rennes"}, user.infopersos)
        # self.assertEqual("Monsieur", user.civilite)
        self.assertEqual("Dylan", user.prenom)
        self.assertEqual("curtetdylan", user.pseudo)

    # Test de la classe Annonce dans le cas où par exemple le salaire maximum
    # serait manquant

    def test_annonce_attributes(self):
        # GIVEN
        annonce_data = {
            "adref": "12345",
            # 'category_label': 'Category',
            "category": {"tag": "Tag", "label": "Category"},
            # 'category_tag': 'Tag',
            # 'company_display_name': 'Company Inc.',
            "company": {"display_name": "Company Inc."},
            "contract_time": "Full-time",
            "created": "2023-10-25 12:00:00",
            "description": "Job description",
            "id": "A1",
            "latitude": 40.7128,
            # 'location_area': ['Area 1', 'Area 2'],
            # 'location_display_name': 'Location',
            "location": {"area": ["Area 1", "Area 2"], "display_name": "Location"},
            "longitude": -74.0060,
            "redirect_url": "https://example.com/job",
            "salary_is_predicted": 1,
            "title": "Job Title",
            "contract_type": "Permanent",
            "salary_min": 30000,
        }
        annonce = Annonce(annonce_data)

        # THEN
        self.assertEqual("12345", annonce.adref)
        self.assertEqual("Category", annonce.category_label)
        self.assertEqual("Tag", annonce.category_tag)
        self.assertEqual("Company Inc.", annonce.company_display_name)
        self.assertEqual("Full-time", annonce.contract_time)
        self.assertEqual("2023-10-25 12:00:00", annonce.created)
        self.assertEqual("Job description", annonce.description)
        self.assertEqual("A1", annonce.id)
        self.assertEqual(40.7128, annonce.latitude)
        self.assertEqual(["Area 1", "Area 2"], annonce.location_area)
        self.assertEqual("Location", annonce.location_display_name)
        self.assertEqual(-74.0060, annonce.longitude)
        self.assertEqual("https://example.com/job", annonce.redirect_url)
        self.assertEqual(1, annonce.salary_is_predicted)
        self.assertEqual("Job Title", annonce.title)
        self.assertEqual("Permanent", annonce.contract_type)
        self.assertEqual(30000, annonce.salary_min)

    def test_recherche_attributes(self):
        # GIVEN
        recherche = Recherche(
            nom="Data Scientist",
            country=PaysEnum.FR,
            page=1,
            params={"param1": "valeur1"},
        )
        # WHEN
        # THEN
        self.assertEqual(recherche.nom, "Data Scientist")
        self.assertEqual(recherche.country, PaysEnum.FR)
        self.assertEqual(recherche.page, 1)
        self.assertEqual(recherche.params, {"param1": "valeur1"})

    def test_resultat_recherche(self):
        # GIVEN
        annonce_data = {
            "adref": "12345",
            "category": {"label": "Category", "tag": "Tag"},
            # 'category_label': 'Category',
            # 'category_tag': 'Tag',
            "company": {"display_name": "Company Inc."},
            # 'company_display_name': 'Company Inc.',
            "contract_time": "Full-time",
            "created": "2023-10-25 12:00:00",
            "description": "Job description",
            "id": "A1",
            "latitude": 40.7128,
            "location": {"area": ["Area 1", "Area 2"], "display_name": "Location"},
            # 'location_area': ['Area 1', 'Area 2'],
            # 'location_display_name': 'Location',
            "longitude": -74.0060,
            "redirect_url": "https://example.com/job",
            "salary_is_predicted": 1,
            "title": "Job Title",
            "contract_type": "Permanent",
            "salary_min": 30000,
        }
        annonce = Annonce(annonce_data)
        recherche = Recherche(
            nom="Data Scientist",
            country=PaysEnum.FR,
            page=1,
            params={"param1": "valeur1"},
        )

        resultat_recherche = ResultatRecherche(
            recherche=recherche,
            nombreAnnonces=2,
            salaireMoyen=2500,
            listeAnnonces=[annonce],
        )
        # WHEN
        # THEN
        self.assertEqual(resultat_recherche.recherche, recherche)
        self.assertEqual(resultat_recherche.nombreAnnonces, 2)
        self.assertEqual(resultat_recherche.salaireMoyen, 2500)
        self.assertEqual(resultat_recherche.listeAnnonces, [annonce])

    def test_candidature_attributes(self):
        candidature = Candidature(
            id_utilisateur=123,
            id_annonce="1234A",
            cv="Mon CV",
            lettre_motivation="je suis très motivé",
        )

        # WHEN - Pas d'action spécifique à effectuer dans ce cas

        # THEN
        self.assertEqual(candidature.cv, "Mon CV")
        self.assertEqual(candidature.lettre_motivation, "je suis très motivé")
        self.assertEqual(candidature.id_utilisateur, 123)
        self.assertEqual(candidature.id_annonce, "1234A")


if __name__ == "__main__":
    unittest.main()
