from view.vue_abstraite import VueAbstraite
from service.utilisateur_recherche import RechercheService
from InquirerPy import prompt
from business_object.recherche import Recherche
from view.passer_page_vue import PasserPageVue
from business_object.pays_enum import PaysEnum,associer_pays
import os
import dotenv


class FaireRechercheVue(VueAbstraite):

    def __init__(self, message=""):
        super().__init__(message)
        self.questions = [
            {"type": "input", "name": "job", "message": "Quel métier recherchez vous ?"},
            {
                "type": "list",
                "name": "pays",
                "message": "Dans quel pays souhaitez-vous réaliser votre recherche ?",
                "choices": [
                    
                    "Afrique du sud",
                    "Allemagne",
                    "Autriche",
                    "Australie",
                    "Belgique",
                    "Brésil",
                    "Canada",
                    "Espagne",
                    "Etats Unis d'Amérique",
                    "France",
                    "Grande-Bretagne",
                    "Inde",
                    "Italie",
                    "Mexique",
                    "Nouvelle Zélande",
                    "Pays-Bas",
                    "Pologne",
                    "Singapour",
                    "Suisse",

                ],
            },  
            {"type": "input", "name": "lieu", "message": "Où voulez vous trouver votre emploi ?"},
            {"type": "input", "name": "distance", "message": "A quelle distance du lieu de recherche acceptez vous que soit votre emploi (en km) ?"},
            {"type": "input", "name": "salaire_min", "message": "Quel salaire minimum voudriez vous ?"},
            {"type": "input", "name": "salaire_max", "message": "Quel salaire maximum voudriez vous ?"},
            {
                "type": "list",
                "name": "rythme",
                "message": "Quel rythme souhaitez vous ?",
                "choices": [
                    "Temps partiel",
                    "Temps plein",
                    "Indifférent",
                ],
            },
            {
                "type": "list",
                "name": "contrat",
                "message": "Quel type de contrat souhaitez vous ?",
                "choices": [
                    "CDI",
                    "CDD",
                    "Indifférent",
                ],
            },
            {
                "type": "list",
                "name": "nbr_annonces",
                "message": "Combien voulez vous voir d'annonces par page ?",
                "choices": [
                    "1",
                    "5",
                    "10",
                    "30",
                ],
            }
            
        ]

    def choisir_menu(self, pseudo=""):

        reponse = prompt(self.questions)

        job = reponse["job"] 
        pays = reponse["pays"]
        pays_recherche= associer_pays(pays)
        lieu = reponse["lieu"]
        distance = reponse["distance"] 
        salaire_min = reponse["salaire_min"] 
        salaire_max = reponse["salaire_max"]
        rythme = reponse["rythme"]  
        contrat = reponse["contrat"] 
        nbr_annonces = reponse["nbr_annonces"]

        dotenv.load_dotenv(override =True)
        app_id = os.environ["APP_ID"]
        app_key = os.environ["APP_KEY"]

        
        if job != "":
            recherche = Recherche(nom = "", country = pays_recherche, page = 1, params={"app_id":app_id,"app_key":app_key,"what": f"{job}","results_per_page":f"{nbr_annonces}"})
            if lieu != "": 
                liste_lieu = []
                liste_lieu.append(lieu)
                resultat = RechercheService().filtrer_lieu_recherche(recherche, liste_lieu)
                recherche = resultat.recherche
            else: 
                resultat = RechercheService().recherche_emploi(recherche)
                recherche = resultat.recherche
        elif lieu != "": 
            recherche = Recherche(nom = "", country=pays_recherche, page = 1, params={"app_id":app_id,"app_key":app_key,"results_per_page":f"{nbr_annonces}"})
            resultat = RechercheService().filtrer_lieu_recherche(recherche, [lieu])
            recherche = resultat.recherche
        else : 
            print("Merci de renseigner un type de métier ou un lieu")
            return FaireRechercheVue().choisir_menu()
        if distance != "":
            distance = int(distance)
            resultat = RechercheService().filtrer_distance_recherche(recherche,distance)
            recherche = resultat.recherche
        if salaire_min != "":
            salaire_min = int(salaire_min)
            resultat = RechercheService().filtrer_salaire_min(recherche,salaire_min)
            recherche = resultat.recherche
        if salaire_max != "":
            salaire_max = int(salaire_max)
            resultat = RechercheService().filtrer_salaire_max(recherche,salaire_max)
            recherche = resultat.recherche
        if rythme == "Temps partiel":
            resultat = RechercheService().filtre_job_temps_partiel(recherche)
            recherche = resultat.recherche
        if rythme == "Temps plein":
            resultat = RechercheService().filtre_job_temps_plein(recherche)
            recherche = resultat.recherche
        if contrat == "CDI":
            resultat = RechercheService().filtre_job_permanent(recherche)
            recherche = resultat.recherche
        if contrat == "CDD":
            resultat = RechercheService().filtre_job_en_contrat(recherche)
            recherche = resultat.recherche
        print(resultat)
        from view.passer_page_vue import PasserPageVue
        nbr_pages = (int(resultat.nombreAnnonces) // int(nbr_annonces)) + 1 
        i = 1
        liste_annonces = resultat.listeAnnonces
        while (i < nbr_pages) :
            i+=1 
            stop, liste_annonce = PasserPageVue().choisir_menu(resultat,recherche, pseudo, liste_annonces)
            if stop ==1:
                break
            else: pass#<return(PasserPageVue().choisir_menu(resultat, recherche, pseudo, liste_annonces))
        if pseudo == "":
            from view.accueil_vue import AccueilVue
            return(AccueilVue().choisir_menu())
        else :
            if i == nbr_pages:
                from view.enregistrer_annonce_vue import EnregistrerAnnonceVue
                EnregistrerAnnonceVue().choisir_menu(liste_annonces, pseudo)               
            from view.menu_utilisateur_vue import MenuUtilisateurVue
            return(MenuUtilisateurVue().choisir_menu(pseudo))
                



if __name__ == "__main__":
    vue = FaireRechercheVue()
    vue.choisir_menu()