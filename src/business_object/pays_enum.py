from enum import Enum


class PaysEnum(Enum):
    FR = "fr"  # France
    GB = "gb"  # Grande-Bretagne
    US = "us"  # Etats Unis d'Amérique
    AT = "at"  # Autriche
    AU = "au"  # Australie
    BE = "be"  # Belgique
    BR = "br"  # Brésil
    CA = "ca"  # Canada
    CH = "ch"  # Suisse
    DE = "de"  # Allemagne
    ES = "es"  # Espagne
    IN = "in"  # Inde
    IT = "it"  # Italie
    MX = "mx"  # Mexique
    NL = "nl"  # Pays-bas
    NZ = "nz"  # Nouvelle Zélande
    PL = "pl"  # Pologne
    SG = "sg"  # Singapour
    ZA = "za"  # Afrique du sud

    def __str__(self):
        return self.value

def associer_pays(pays_str):
    if pays_str == "France":
        return PaysEnum.FR
    elif pays_str == "Grande-Bretagne":
        return PaysEnum.GB
    elif pays_str == "Etats Unis d'Amérique":
        return PaysEnum.US
    elif pays_str == "Autriche":
        return PaysEnum.AT
    elif pays_str == "Australie":
        return PaysEnum.AU
    elif pays_str == "Belgique":
        return PaysEnum.BE
    elif pays_str == "Brésil":
        return PaysEnum.BR
    elif pays_str == "Canada":
        return PaysEnum.CA
    elif pays_str == "Suisse":
        return PaysEnum.CH
    elif pays_str == "Allemagne":
        return PaysEnum.DE
    elif pays_str == "Espagne":
        return PaysEnum.ES
    elif pays_str == "Inde":
        return PaysEnum.IN
    elif pays_str == "Italie":
        return PaysEnum.IT
    elif pays_str == "Mexique":
        return PaysEnum.MX
    elif pays_str == "Pays-Bas":
        return PaysEnum.NL
    elif pays_str == "Nouvelle Zélande":
        return PaysEnum.NZ
    elif pays_str == "Pologne":
        return PaysEnum.PL
    elif pays_str == "Singapour":
        return PaysEnum.SG
    elif pays_str == "Afrique du sud":
        return PaysEnum.ZA
    else:
        raise ValueError(f"Le pays '{pays_str}' n'a pas été trouvé dans l'énumération PaysEnum.")
        

