from view.vue_abstraite import VueAbstraite
from service.utilisateur_service import UtilisateurService

from InquirerPy import prompt


class CreationCompteVue(VueAbstraite):
    def __init__(self):
        message = "Créer un compte"
        super().__init__(message)
        self.questions =  [
            {"type": "input", "name": "id", "message": "Entrez votre identifiant :"},
            {"type": "password", "name": "mdp1", "message": "Entrez votre mot de passe :"},
            {"type": "password", "name": "mdp2", "message": "Confirmez votre mot de passe :"},
        ]
 
    def choisir_menu(self):
        answers = prompt(self.questions)
        id = answers["id"]
        mdp1 = answers["mdp1"]
        mdp2 = answers["mdp2"]
        print(UtilisateurService().creer_compte(id, mdp1, mdp2))
        from view.accueil_vue import AccueilVue
        return AccueilVue().choisir_menu()


if __name__ == "__main__":
    vue = CreationCompteVue()
    vue.choisir_menu()

