INSERT INTO jobhub.utilisateur(pseudo, mdp_hashe, nom, prenom, adresse, mail, tel, preferences) VALUES
    ('jeanmi',
     '623e3e15c9469506c4be8d9756feed155e98321b4a300a656f690184a866b92a', --jeanmi
     'Statistiques', 
     'Jean-Michel', 
     '18 rue du paradis 35000 Rennes', 
     'jean-michel.statistiques@insee.fr', 
     '02 27 28 37 56', 
     '{"metier": "data scientist", "localisation": "Rennes", "contrat": "1", "salaire_min": 0}'  ),
     ('lara',
     '2b157696b7b7c7eb0bbe87ad27cc76c3a066f8bd0f61e5d4b3ee8c9a6882d5c5', --lara
     'Pafromage',
     'Lara',
     '4 boulevard François Coquet 46500 Rocamadour',
     'lara.pafromage@yahoo.com',
     '06 31 41 59 26 53',
     '{"metier": "coach", "localisation": "Lyon", "contrat": "1", "salaire_min": 0 }');

insert into jobhub.annonce(adref, category_label, category_tag,
    company_display_name, contract_type,created, description, id_annonce, latitude, 
    location_area, location_display_name, longitude, redirect_url,
    salary_is_predicted,  salary_max,
    salary_min, title) VALUES
    ('eyJhbGciOiJIUzI1NiJ9.eyJpIjoiNDI4NDQ2ODY4MCIsInMiOiJkZ29QMWNxSzdoR0NyQzFpQWtoTDd3In0.O4vW-uQuST8ESBJzOXlpuD714sOp58CTqUKrSi9OI-8',
     'Emplois Autres/Général',
     'other-general-jobs',
     'WAVY SERVICES',
     '',
     '2023-08-30T08:34:38Z',
     'Nous recrutons un spécialiste Flux Data H/F basé à Rennes. Au sein du pôle DATA dans une DSI à taille humaine (environ 30 personnes), vous interviendrez, dans un premier temps, sur la rationalisation des flux existants et leur modernisation. Missions principales - Gérer les sujets d interfaçages inter-applicatifs du groupe - Etre l interlocuteur des métiers pour cadrer les besoins et les traduire techniquement - Piloter un portefeuille de projets - Organiser le travail à l aide des ressources i…',
     '4284468680',
     48.11176,
     '{"France", "Bretagne", "Ille-et-Vilaine", "Rennes", "Betton"}',
     'Betton, Rennes',
     -1.68027,
     'https://www.adzuna.fr/land/ad/4284468680?se=dgoP1cqK7hGCrC1iAkhL7w&utm_medium=api&utm_source=4aae40b4&v=BB8B9138017DC1EA074E575CA6B6A992AA553AF3',
     '0',
     53000,
     48000,
     'Spécialiste Flux Data H/F'),
     (
        'eyJhbGciOiJIUzI1NiJ9.eyJzIjoiZGdvUDFjcUs3aEdDckMxaUFraEw3dyIsImkiOiI0NDQ0MzY1OTY1In0.CUbBDT8MtY-FVkO4IUfWdJ0ajyxZCMuJkfHUzqJUY_s',
        'Emplois Comptabilité et Finance',
        'accounting-finance-jobs',
        'ELSYS Design',
        '',
        '2023-11-21T06:53:14Z',
        'Contexte et lieu de mission : ELSYS Design recherche un ingénieur Data Analyst dans le cadre d une collaboration avec un de nos clients stratégique du secteur Ferroviaire. Dans ce contexte, vous serez amené à intégrer son équipe Data à Tours et participerez à un projet d envergure sur la maintenance (Corrective/Prédictive) par l analyse et l interprétation des données issues des outils communicants. Responsabilités : - Analyse et interprétation de données - Écriture d équations de code - Suppor…',
        '4444365965',
        48.11842,
        '{
                    "France",
                    "Bretagne",
                    "Ille-et-Vilaine",
                    "Rennes",
                    "Cesson-Sévigné"
                }',
        'Cesson-Sévigné, Rennes',
        -1.60461,
        'https://www.adzuna.fr/land/ad/4444365965?se=dgoP1cqK7hGCrC1iAkhL7w&utm_medium=api&utm_source=4aae40b4&v=61F687DA20FB2FD770AB4EF3705635D418ED2C68',
        '0',
        55000,
        35000,
        'Ingénieur Data Analyse H/F'
     );

INSERT INTO jobhub.candidature(id_utilisateur, id_annonce) VALUES
    (1, '4284468680');

insert into jobhub.recherche(id_utilisateur, nom_recherche, country,
    page, params) VALUES
    (1,
    'data rennes',
    'fr',
    1,
    '{"what": "data", "where": "Rennes", "results_per_page": 2}');

insert into jobhub.resultat_recherche(id_recherche, nombre_annonces,
    salaire_moyen, liste_id_annonces) VALUES
    (1,
    362,
    45989.79,
    '{"4284468680", "4444365965"}');
