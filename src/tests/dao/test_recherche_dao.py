import unittest
from utils.reset_database import ResetDatabase
from dao.recherche_dao import RechercheDAO
from dao.resultat_recherche_dao import ResultatRechercheDAO
from dao.db_connection import DBConnection
from business_object.recherche import Recherche
from business_object.pays_enum import PaysEnum
import psycopg2.errors


class RechercheDaoTest(unittest.TestCase):
    def setUp(cls):
        DBConnection.CONNECTION_TO_TEST_DB = True
        ResetDatabase().lancer()

    def tearDown(cls):
        DBConnection.CONNECTION_TO_TEST_DB = False

    def test_recherche_existe(self):
        # GIVEN
        nom_recherche = "data rennes"
        faux_nom_recherche = "pouet pouet"
        id_utilisateur = 1
        # WHEN
        oui = RechercheDAO().recherche_existe(nom_recherche, id_utilisateur)
        non = RechercheDAO().recherche_existe(faux_nom_recherche, id_utilisateur)
        # THEN
        self.assertTrue(oui)
        self.assertFalse(non)

    def test_creation_recherche(self):
        # GIVEN
        recherche_deja_existante = Recherche("data rennes", PaysEnum.FR, 1)
        recherche = Recherche("nouvelle recherche", PaysEnum.FR, 1)
        # WHEN
        success = RechercheDAO().creation_recherche(recherche, 1)
        # THEN
        self.assertTrue(success)
        with self.assertRaises(psycopg2.errors.UniqueViolation):
            RechercheDAO().creation_recherche(recherche_deja_existante, 1)

    def test_trouver_recherche_par_nom_et_utilisateur(self):
        # GIVEN
        nom_recherche = "data rennes"
        faux_nom_recherche = "pouet pouet"
        id_utilisateur = 1
        # WHEN
        recherche_dao = RechercheDAO().trouver_recherche_par_nom_et_utilisateur(
            nom_recherche, id_utilisateur
        )
        recherche_none = RechercheDAO().trouver_recherche_par_nom_et_utilisateur(
            faux_nom_recherche, id_utilisateur
        )
        # THEN
        self.assertEqual(nom_recherche, recherche_dao.nom)
        self.assertIsNone(recherche_none)

    def test_trouver_recherche_par_id_recherche(self):
        # GIVEN
        # WHEN
        recherche1 = RechercheDAO().trouver_recherche_par_id_recherche(1)
        recherche_none = RechercheDAO().trouver_recherche_par_id_recherche(2)
        # THEN
        self.assertEqual("data rennes", recherche1.nom)
        self.assertIsNone(recherche_none)

    def test_supprime_recherche(self):
        # GIVEN
        recherche_deja_existante = Recherche("data rennes", PaysEnum.FR, 1)
        recherche_deja_existante.id_recherche = 1
        recherche_inexistante = Recherche("nouvelle recherche", PaysEnum.FR, 1)
        # WHEN
        # On vérifie que la suppression de la recherche
        # supprime également son résultat
        resultat_recherche_existe = (
            ResultatRechercheDAO().trouver_resultat_recherche_par_id_recherche(1)
        )
        success = RechercheDAO().supprime_recherche(recherche_deja_existante)
        non = RechercheDAO().recherche_existe(recherche_deja_existante.nom, 1)
        resultat_recherche_nexiste_plus = (
            ResultatRechercheDAO().trouver_resultat_recherche_par_id_recherche(1)
        )

        fail = RechercheDAO().supprime_recherche(recherche_inexistante)

        # THEN
        self.assertIsNotNone(resultat_recherche_existe)
        self.assertTrue(success)
        self.assertFalse(non)
        self.assertIsNone(resultat_recherche_nexiste_plus)
        self.assertFalse(fail)

    def test_trouver_noms_recherche_pour_id_utilisateur(self):
        # GIVEN
        # WHEN
        liste1_noms = RechercheDAO().trouver_noms_recherche_pour_id_utilisateur(1)
        liste2_noms = RechercheDAO().trouver_noms_recherche_pour_id_utilisateur(2)
        # THEN
        self.assertEqual(["data rennes"], liste1_noms)
        self.assertEqual([], liste2_noms)

    def test_trouver_toutes_recherches_pour_id_utilisateur(self):
        # GIVEN
        # WHEN
        liste1_recherches = (
            RechercheDAO().trouver_toutes_recherches_pour_id_utilisateur(1)
        )
        liste2_recherches = (
            RechercheDAO().trouver_toutes_recherches_pour_id_utilisateur(2)
        )
        # THEN
        self.assertEqual(1, len(liste1_recherches))
        self.assertEqual(0, len(liste2_recherches))


if __name__ == "__main__":
    unittest.main()
