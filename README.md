# JobHub

## Description

A python application to find a job! Find the job that suits you and prepare your application.


## Creation of the local repository

In a git bash terminal fill the following link to clone the project either with SSH :
```bash
git clone git@gitlab.com:Rania.f/projet_info_jobhub.git
```
or with HTTPS:
```bash
git clone https://gitlab.com/Rania.f/projet_info_jobhub.git
```

## Visual Studio Code 

* File > Open Folder > **projet_info_jobhub-main**


## Required packages to install

The packages necessary for the proper functioning of the application are :
* [inquirerPy](https://inquirerpy.readthedocs.io/en/latest/)
* [Requests](https://requests.readthedocs.io/en/latest/)
* [psycopg2_binary](https://www.psycopg.org/docs/)
* [python-dotenv](https://pypi.org/project/python-dotenv/)
* [emoji](https://pypi.org/project/emoji/)
* [colorama](https://pypi.org/project/colorama/)


In a git bash terminal write the following instruction to install all packages which are required for the application :
```bash
pip install -r requirements.txt
pip list
```


## Create a `.env` file
If ther is not a file called `.env` at the root of the project, create it and fill in the following content in this file :

```
HOST_WEBSERVICE=http://web-services.domensai.ecole

HOST=sgbd-eleves.domensai.ecole
PORT=5432
DATABASE=id2236
USER=id2236
PASSWORD=id2236

DATABASETEST=id2337
USERTEST=id2337
PASSWORDTEST=id2337

APP_ID=4aae40b4
APP_KEY=2126d1c6c03226a595566bb3d7579b7a
```

## Create a PostgreSQL database 
Create the database and fill in the following attributes in the `.env` file :
- HOST
- PASSWORD
- USER
- DATABASE
- PORT

On the same host and port, create a copy of this database for the tests with the following attributes of the `.env` file :
- DATABASETEST
- USERTEST
- PASSWORDTEST

## How to create and pop the databases

You will find the following files in the **data** folder :
* `init_db.sql` to create schema and tables
* `pop_db.sql` to pop tables with a data sample

Both databases (production and tests) have the same schema.

You can also use this command to call those above scripts
```bash
python src/utils/reset_both_databases.py       # to create schema and tables and then pop tables with a data sample in both databases (prod and tests)
```


## How to launch the app

In a git bash terminal write the following instruction to launch the app or to run all of the tests
```bash
python src/main.py                       # to launch the app
python src/run_all_tests.py              # to launch all of the tests
python -m unittest
```
