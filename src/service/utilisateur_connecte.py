from dao.utilisateur_dao import UtilisateurDAO
from business_object.utilisateur import Utilisateur
from utils.hachage import Hachage
from business_object.candidature import Candidature
from dao.candidature_dao import CandidatureDAO
from dao.annonce_dao import AnnonceDAO
from dao.resultat_recherche_dao import ResultatRechercheDAO
from business_object.resultat_recherche import ResultatRecherche
from business_object.recherche import Recherche
import os
from service.utilisateur_recherche import RechercheService
import dotenv

class UtilisateurConnecte(Utilisateur):
    def __init__(self, pseudo, mdp_hashe, nom, preferences, adresse, prenom, mail, tel):
        super().__init__(
            pseudo, mdp_hashe, nom, preferences, adresse, prenom, mail, tel
        )

    def modifier_infos_perso(self, nom, prenom, mail, tel, adresse):
        """Permet à l'utilisateur d'ajouter ses informations personnelles ou de les modifier.

        Parameters
        ----------
        nom
            Nom de l'utilisateur
        prenom
            Prénom de l'utilisateur
        mail
            Mail de l'utilisateur
        tel
            Numéro de téléphone de l'utilisateur
        adresse
            Adresse postale de l'utilisateur

        Returns
        -------
        str
            Confirmation que la modification a été effectuée ou erreur

        """
        if nom != "":
            self.nom = nom
        if prenom != "":
            self.prenom = prenom
        if mail != "":
            self.mail = mail
        if tel != "":
            self.tel = tel
        if adresse != "":
            self.adresse = adresse
        if UtilisateurDAO().miseajour_utilisateur(self):
            return "Vos informations ont été modifiées !"
        else:
            return "L'utilisateur n'est peut être pas connecté et ne peut donc pas réaliser cette opération."

    def modifier_mdp(self, pseudo, mdp, new_mdp):
        """Permet à l'utilisateur de modifier le mot de passe qui sécurise son compte.
        L'utilisateur doit confirmer son identité avant de réaliser cette opération.

        Parameters
        ----------
        pseudo
            Pseudonyme de l'utilisateur
        mdp
            Ancien mot de passe à changer de l'utilisateur
        new_mdp
            Nouveau mot de passe de l'utilisateur

        Returns
        -------
        str
            Confirmation que le mot de passe a été modifié ou erreur à corriger

        """
        if pseudo == self.pseudo:
            mdp_hache = Hachage().sel_hachage(pseudo, mdp)
            if self.mdp_hashe == mdp_hache:
                new_mdp_hache = Hachage().sel_hachage(pseudo, new_mdp)
                self.mdp_hashe = new_mdp_hache
                if UtilisateurDAO().miseajour_utilisateur(self):
                    return "Votre mot de passe a été modifié !"
            else:
                return "Vous vous êtes trompé de mot de passe."
        return "Vous vous êtes trompé de pseudo."

    def modifier_preferences(self, metier="", localisation="", salaire_min="", contrat=""):
        """Permet à l'utilisateur d'ajouter ses préférences ou de les modifier.

        Parameters
        ----------
        metier
            Métier recherché par l'utilisateur
        contrat
            Type de contrat recherché par l'utilisateur
        localisation
            Localisation recherchée par l'utilisateur
        salaire_min
            Salaire minimum acceptable pour l'utilisateur

        Returns
        -------
        str
            Confirmation que la modification a été effectuée ou erreur

        """
        dotenv.load_dotenv(override=True)
        app_id=os.environ["APP_ID"]
        app_key= os.environ["APP_KEY"]
        dic = dict(
            metier=metier,
            contrat=contrat,
            localisation=localisation,
            salaire_min=salaire_min,
            nbr_annonce="",
        )
        self.preferences = dic
        if UtilisateurDAO().miseajour_utilisateur(self):
            job = metier
            lieu = localisation
            contrat = contrat 
            salaire_min = salaire_min
            #recherche =""
            self.preferences = dic
            UtilisateurDAO().miseajour_utilisateur(self)

            #if job != "":
            #    recherche = Recherche(nom = "", country = "fr", page = 1, params={"app_id":app_id,"app_key":app_key,"what": f"{job}","results_per_page":"30"})
            #    if lieu != "": 
            #        liste_lieu = []
            #        liste_lieu.append(lieu)
            #        resultat = RechercheService().filtrer_lieu_recherche(recherche, liste_lieu)
            #        recherche = resultat.recherche
            #elif lieu != "": 
            #    recherche = Recherche(nom = "", country = "fr", page = 1, params={"app_id":app_id,"app_key":app_key,"what": f"{lieu}","results_per_page":"30"})
            #if salaire_min != "" and recherche != "":
            #    salaire_min = int(salaire_min)
            #    resultat = RechercheService().filtrer_salaire_min(recherche,salaire_min)
            #    recherche = resultat.recherche
            #if contrat == "CDI"and recherche != "":
            #    resultat = RechercheService().filtre_job_permanent(recherche)
            #    recherche = resultat.recherche
            #if contrat == "CDD"and recherche != "":
            #    resultat = RechercheService().filtre_job_en_contrat(recherche)
            #    recherche = resultat.recherche
            #if recherche == "":
            #    pass
            #else :
            #    from dao.recherche_dao import RechercheDAO
            #    id_utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(self.pseudo).id_utilisateur
            #    RechercheDAO().creation_recherche(recherche, id_utilisateur)
            #    resultat = RechercheService().filtre_job_en_contrat(recherche)
            #    dic["nbr_annonce"]=str(resultat.nombreAnnonces)
            #    self.preferences = dic
            #    UtilisateurDAO().miseajour_utilisateur(self)
        else:
            return "L'utilisateur n'est peut être pas connecté et ne peut donc pas réaliser cette opération."

    def supprimer_compte(self):
        """Permet à l'utilisateur de supprimer son compte.

        Returns
        -------
        str
            Confirmation que la suppression a été effectuée

        """
        UtilisateurDAO().supprime_utilisateur(self)
        return "Votre compte à été supprimé !"

    def voir_profil(self):
        message = f"Voici votre profil utilisateur {self.pseudo} ! \n Vous êtes {self.prenom} {self.nom}. \n Vos informations sont les suivantes : \n Adresse : {self.adresse} \n Mail : {self.mail} \n Téléphone : {self.tel} \n Préférences : {self.preferences}"
        print(message)

    def enregistrer_annonce(self, annonce, cv="", lettre=""):
        if AnnonceDAO().annonce_existe(annonce) == False:
            AnnonceDAO().creation_annonce(annonce)
        id_utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(self.pseudo).id_utilisateur
        id_annonce = annonce.id
        candidature = Candidature(id_utilisateur= id_utilisateur, id_annonce = id_annonce, cv=cv, lettre_motivation=lettre)
        return(CandidatureDAO().creation_candidature(candidature))

    def voir_annonces_enregistrees(self):
        id_utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(self.pseudo).id_utilisateur
        liste_candidatures = CandidatureDAO().trouver_toutes_candidatures_pour_id_utilisateur(id_utilisateur)
        if liste_candidatures != [] :
            for i in liste_candidatures:
                id_annonce = i.id_annonce
                annonce = AnnonceDAO().trouver_annonce_par_id(id_annonce)
                print(annonce)
                if i.cv != "":
                    print(f"Le chemin de votre CV est :{i.cv}")
                if i.lettre_motivation !="":
                    print(f"Le chemin de votre lettre de motivation est :{i.lettre_motivation}")
        else:
            print("Vous n'avez pas enregistré d'annonce !")


    def alerte(self):
        dotenv.load_dotenv(override=True)
        app_id=os.environ["APP_ID"]
        app_key= os.environ["APP_KEY"]
        preferences = self.preferences
        if preferences != {}:
            job = preferences["metier"]
            lieu = preferences["localisation"]
            contrat = preferences["contrat"] 
            salaire_min = preferences["salaire_min"]
            nbr_annonce_preference = preferences["nbr_annonce"]
            recherche =""

            if job != "":
                recherche = Recherche(nom = "", country = "fr", page = 1, params={"app_id":app_id,"app_key":app_key,"what": f"{job}","results_per_page":"30"})
                if lieu != "": 
                    liste_lieu = []
                    liste_lieu.append(lieu)
                    resultat = RechercheService().filtrer_lieu_recherche(recherche, liste_lieu)
                    recherche = resultat.recherche
            elif lieu != "": 
                recherche = Recherche(nom = "", country = "fr", page = 1, params={"app_id":app_id,"app_key":app_key,"what": f"{lieu}","results_per_page":"50"})
            if salaire_min != "" and recherche != "":
                salaire_min = int(salaire_min)
                resultat = RechercheService().filtrer_salaire_min(recherche,salaire_min)
                recherche = resultat.recherche
            if contrat == "CDI"and recherche != "":
                resultat = RechercheService().filtre_job_permanent(recherche)
                recherche = resultat.recherche
            if contrat == "CDD"and recherche != "":
                resultat = RechercheService().filtre_job_en_contrat(recherche)
                recherche = resultat.recherche
            if recherche == "":
                print("Aucune annonce ne correspond à vos préférences !")
            else :
                resultat = RechercheService().filtre_job_en_contrat(recherche)
                if str(resultat.nombreAnnonces) !=  nbr_annonce_preference : 
                    print("De nouvelles annonces correspondant à vos préférences ont été postées !")
                else :
                    print("Aucune nouvelle annonce correspondant à vos préférences n'a été postée !")
        else :
            print("Si vous voulez recevoir des alertes concernant les nouvelles annonces remplissez vos préférences.")



if __name__ == "__main__":
    pseudo = "id10"
    mdp = Hachage().sel_hachage("id10", "mdp10")
    utilisateur = UtilisateurConnecte(pseudo, mdp, "", "", "", "", "", "")
    print(utilisateur.enregistrer_annonce(0))
    print(
        utilisateur.modifier_infos_perso(
            nom="lele", prenom="", mail="", tel="", adresse=""
        )
    )
    print(utilisateur.modifier_preferences(metier="codeur"))
    print(utilisateur.modifier_mdp("id10", "mdp10", "mdp11"))
    print(utilisateur.modifier_mdp("id10", "mdp10", "mdp11"))
    print(utilisateur.modifier_mdp("id11", "mdp11", "mdp12"))
    print(utilisateur)
