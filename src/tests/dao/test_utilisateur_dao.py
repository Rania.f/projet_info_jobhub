import unittest
from utils.reset_database import ResetDatabase
from dao.utilisateur_dao import UtilisateurDAO
from dao.db_connection import DBConnection
from business_object.utilisateur import Utilisateur
import psycopg2.errors


class UtilisateurDaoTest(unittest.TestCase):
    def setUp(cls):
        DBConnection.CONNECTION_TO_TEST_DB = True
        ResetDatabase().lancer()

    def tearDown(cls):
        DBConnection.CONNECTION_TO_TEST_DB = False

    def test_pseudo_existe(self):
        # GIVEN
        oui = UtilisateurDAO().pseudo_existe("jeanmi")
        non = UtilisateurDAO().pseudo_existe("Roger")
        # WHEN
        # THEN
        self.assertTrue(oui)
        self.assertFalse(non)

    def test_creation_utilisateur(self):
        # GIVEN
        utilisateur = Utilisateur("Raymonde", "mdp")
        success = UtilisateurDAO().creation_utilisateur(utilisateur)

        # WHEN
        # THEN
        # On vérifie que raymonde a bien étée crée
        self.assertTrue(success)

        # On teste qu'une tentative de créer une deuxième Raymonde provoque une erreur
        with self.assertRaises(psycopg2.errors.UniqueViolation):
            UtilisateurDAO().creation_utilisateur(utilisateur)

    def test_trouver_tous_pseudo(self):
        # GIVEN
        # ResetDatabase().lancer()
        pseudos = UtilisateurDAO().trouver_tous_pseudo()
        # THEN
        self.assertEqual(["jeanmi", "lara"], pseudos)

    def test_trouver_tous_utilisateurs(self):
        # GIVEN
        # ResetDatabase().lancer()
        utilisateurs = UtilisateurDAO().trouver_tous_utilisateurs()
        # THEN
        self.assertEqual(2, len(utilisateurs))

    def test_trouver_utilisateur_par_pseudo(self):
        # GIVEN
        success = UtilisateurDAO().trouver_utilisateur_par_pseudo("jeanmi")
        fail = UtilisateurDAO().trouver_utilisateur_par_pseudo("momo")
        # THEN
        self.assertTrue(success)
        self.assertIsNone(fail)

    def test_miseajour_utilisateur(self):
        # GIVEN
        jeanmi = UtilisateurDAO().trouver_utilisateur_par_pseudo("jeanmi")
        jeanmi.prenom = "michelle"
        success = UtilisateurDAO().miseajour_utilisateur(jeanmi)

        # THEN
        self.assertTrue(success)

    def test_supprime_utilisateur(self):
        # GIVEN
        jeanmi = UtilisateurDAO().trouver_utilisateur_par_pseudo("jeanmi")
        success = UtilisateurDAO().supprime_utilisateur(jeanmi)

        # THEN
        self.assertTrue(success)


if __name__ == "__main__":
    unittest.main()
