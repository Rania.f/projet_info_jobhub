from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
import emoji


class AccueilVue(VueAbstraite):
    """Vue de l'accueil de l'application JobHub

    Attributes
    ----------
    message=''
        str

    Returns
    ------
    view
        retourne la prochaine vue, celle qui est choisie par l'utilisateur de l'application
    """

    def __init__(self, message="") -> None:
        super().__init__(message)
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Que voulez vous faire ?",
                "choices": [
                    "\U0001f58a\uFE0F Créer un compte",
                    "\U0001f513 Se connecter",
                    "\U0001f50d Faire une recherche",
                    "\U0001f51a Quitter",
                ],
            }
        ]

    def afficher(self):
        titlescreen_file = open("data/titlescreen.txt", "r", encoding="utf-8")
        titlescreen = titlescreen_file.read()
        print(titlescreen)
        pass

    def choisir_menu(self):
        """Choix du menu suivant l'utilisateur

        Return
        ------
        vue
            Retourne la vue choisie par l'utilisateur dans le terminal
        """
        #self.nettoyer_console()
        
        # Ouvrir le fichier en lecture
        #titlescreen_file = open("data/titlescreen.txt", "r", encoding="utf-8")
        #titlescreen = titlescreen_file.read()
        #print(titlescreen)
        
        print(self.message)
        print("Menu:")

        reponse = prompt(self.questions)

        if reponse["choix"] == "\U0001f513 Se connecter":
            from view.connexion_vue import ConnexionVue
            return ConnexionVue().choisir_menu()

        elif reponse["choix"] == "\U0001f58a\uFE0F Créer un compte":
            from view.creation_compte_vue import CreationCompteVue
            return CreationCompteVue().choisir_menu()

        elif reponse["choix"] == "\U0001f50d Faire une recherche":
            from view.faire_recherche_vue import FaireRechercheVue
            pseudo = ""
            return FaireRechercheVue().choisir_menu(pseudo)

        elif reponse["choix"] == "\U0001f51a Quitter":
            pass



if __name__ == "__main__":
    vue = AccueilVue()
    vue.choisir_menu()