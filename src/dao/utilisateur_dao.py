from typing import List
from utils.singleton import Singleton
from business_object.utilisateur import Utilisateur

from dao.db_connection import DBConnection

import json


class UtilisateurDAO(metaclass=Singleton):
    def pseudo_existe(self, pseudo) -> bool:
        """renvoie true si le pseudo existe dans la base,
        false sinon"""

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT EXISTS(                           "
                    "  SELECT 1                               "
                    "  FROM jobhub.utilisateur                "
                    "  WHERE pseudo = %(pseudo)s);            ",
                    {"pseudo": pseudo},
                )

                res = cursor.fetchone()

        return res["exists"]

    def creation_utilisateur(self, utilisateur: Utilisateur) -> bool:
        """Ajoute un nouvel utilisateur dans la table utilisateur"""
        created = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO jobhub.utilisateur (pseudo, mdp_hashe, preferences) "
                    "VALUES (%(pseudo)s, %(mdp_hashe)s, %(preferences_vides)s)       "
                    "RETURNING id_utilisateur;                                       ",
                    {
                        "pseudo": utilisateur.pseudo,
                        "mdp_hashe": utilisateur.mdp_hashe,
                        "preferences_vides": "{}",
                    },
                )
                res = cursor.fetchone()
        if res:
            created = True
            utilisateur.id_utilisateur = res["id_utilisateur"]

        return created

    def trouver_tous_pseudo(self) -> List[str]:
        """Renvoie une liste de tous les pseudos de la base utilisateur"""
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT *                                  "
                    "  FROM jobhub.utilisateur                "
                )
                res = cursor.fetchall()

        pseudos: List[str] = []

        if res:
            for row in res:
                pseudos.append(row["pseudo"])

        return pseudos

    def trouver_tous_utilisateurs(self) -> List[Utilisateur]:
        """Renvoie une liste de tous les utilisateurs"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                         "
                        "   FROM jobhub.utilisateur       "
                    )
                    res = cursor.fetchall()
        except Exception as e:
            print(e)
            raise

        liste_utilisateurs = []

        if res:
            for row in res:
                utilisateur = Utilisateur(
                    pseudo=row["pseudo"],
                    mdp_hashe=row["mdp_hashe"],
                    nom=row["nom"],
                    prenom=row["prenom"],
                    adresse=row["adresse"],
                    mail=row["mail"],
                    tel=row["tel"],
                    preferences=json.loads(row["preferences"]),
                )
                utilisateur.id_utilisateur = row["id_utilisateur"]

                liste_utilisateurs.append(utilisateur)

        return liste_utilisateurs

    def trouver_utilisateur_par_pseudo(self, pseudo: str) -> Utilisateur:
        """renvoie l'utilisateur ayant pseudo pour pseudo"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                  "
                        "  FROM jobhub.utilisateur                 "
                        "  WHERE pseudo = %(pseudo)s               ",
                        {
                            "pseudo": pseudo,
                        },
                    )
                    res = cursor.fetchone()
        except Exception as e:
            print(e)
            raise

        utilisateur = None
        if res:
            utilisateur = Utilisateur(
                pseudo=res["pseudo"],
                mdp_hashe=res["mdp_hashe"],
                nom=res["nom"],
                prenom=res["prenom"],
                adresse=res["adresse"],
                mail=res["mail"],
                tel=res["tel"],
                preferences=json.loads(res["preferences"]),
            )
            utilisateur.id_utilisateur = res["id_utilisateur"]

        return utilisateur

    def miseajour_utilisateur(self, utilisateur: Utilisateur) -> bool:
        updated = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE jobhub.utilisateur                            "
                    "   SET mdp_hashe = %(mdp_hashe)s,                    "
                    "       nom = %(nom)s,                                "
                    "       prenom = %(prenom)s,                          "
                    "       adresse = %(adresse)s,                        "
                    "       preferences = %(preferences)s,                "
                    "       mail = %(mail)s,                              "
                    "       tel = %(tel)s                                 "
                    " WHERE pseudo = %(pseudo)s                           ",
                    {
                        "pseudo": utilisateur.pseudo,
                        "mdp_hashe": utilisateur.mdp_hashe,
                        "nom": utilisateur.nom,
                        "prenom": utilisateur.prenom,
                        "adresse": utilisateur.adresse,
                        "preferences": json.dumps(utilisateur.preferences),
                        "mail": utilisateur.mail,
                        "tel": utilisateur.tel,
                    },
                )
                if cursor.rowcount:
                    updated = True
        return updated

    def supprime_utilisateur(self, utilisateur: Utilisateur) -> bool:
        deleted = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "delete from jobhub.utilisateur                     "
                    " WHERE pseudo = %(pseudo)s                         ",
                    {
                        "pseudo": utilisateur.pseudo,
                    },
                )
                if cursor.rowcount:
                    deleted = True
        return deleted
