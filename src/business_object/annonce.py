import math
from colorama import init, Fore, Style


class Annonce:
    def __init__(self, annonceJson: dict) -> None:
        self.adref = annonceJson["adref"]
        if "category" not in annonceJson:
            self.category_label = ""
            self.category_tag = ""
        else:
            self.category_label = annonceJson["category"]["label"]
            self.category_tag = annonceJson["category"]["tag"]
        if "company" not in annonceJson:
            self.company_display_name = ""
        else:
            if "display_name" not in annonceJson["company"]:
                self.company_display_name = ""
            else:
                self.company_display_name = annonceJson["company"]["display_name"]
        if "contract_time" not in annonceJson:
            annonceJson["contract_time"] = ""
        self.contract_time = annonceJson["contract_time"]
        self.created = annonceJson["created"]
        self.description = annonceJson["description"]
        self.id = annonceJson["id"]
        if "latitude" not in annonceJson:
            annonceJson["latitude"] = float("nan")
        self.latitude = annonceJson["latitude"]
        self.location_area = annonceJson["location"]["area"]
        self.location_display_name = annonceJson["location"]["display_name"]
        if "longitude" not in annonceJson:
            annonceJson["longitude"] = float("nan")
        self.longitude = annonceJson["longitude"]
        self.redirect_url = annonceJson["redirect_url"]
        self.salary_is_predicted = annonceJson["salary_is_predicted"]
        self.title = annonceJson["title"]
        if "contract_type" not in annonceJson:
            annonceJson["contract_type"] = ""
        self.contract_type = annonceJson["contract_type"]
        if "salary_max" not in annonceJson:
            annonceJson["salary_max"] = float("nan")
        self.salary_max = annonceJson["salary_max"]
        if "salary_min" not in annonceJson:
            annonceJson["salary_min"] = float("nan")
        self.salary_min = annonceJson["salary_min"]

    def __str__(self) -> str:
        init(convert=True)
        bold = "\033[1m"
        underline = "\033[4m"
        reset = "\033[0m"
        salary_min_str = (
            "non renseigné" if math.isnan(self.salary_min) else str(self.salary_min)
        )
        salary_max_str = (
            "non renseigné" if math.isnan(self.salary_max) else str(self.salary_max)
        )
        latitude_str = (
            "non renseignée" if math.isnan(self.latitude) else str(self.latitude)
        )
        longitude_str = (
            "non renseignée" if math.isnan(self.longitude) else str(self.longitude)
        )
        contract_time_str = (
            "non renseigné" if self.contract_time == "" else str(self.contract_time)
        )
        contract_type_str = (
            "non renseigné" if self.contract_type == "" else str(self.contract_type)
        )
        company_str = (
            "non renseignéé"
            if self.company_display_name == ""
            else str(self.company_display_name)
        )
        salaire_predit_str = (
            "non renseigné"
            if self.salary_is_predicted == "0"
            else str(self.salary_is_predicted)
        )

        return (
            f"{Style.BRIGHT}{Fore.MAGENTA}Titre:{Style.RESET_ALL}{self.title} \n"
            f"{Style.BRIGHT}{Fore.CYAN}Identifiant : {Style.RESET_ALL}{self.id} \n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Lieu: {Style.RESET_ALL}{self.location_display_name}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Entreprise:{Style.RESET_ALL} {company_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Type de contrat:{Style.RESET_ALL} {contract_type_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Salaire min:{Style.RESET_ALL}{salary_min_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Salaire max{Style.RESET_ALL}:{salary_max_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Description: {Style.RESET_ALL}{self.description.rstrip('…')+ '.'}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Durée du contrat:{Style.RESET_ALL}{contract_time_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Créé le:{Style.RESET_ALL} {self.created}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Latitude:{Style.RESET_ALL}{latitude_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Longitude:{Style.RESET_ALL}{longitude_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}URL de redirection:{Style.RESET_ALL} {Fore.BLUE}{self.redirect_url}{Style.RESET_ALL}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Salaire prédit: {Style.RESET_ALL}{salaire_predit_str}\n"
            f"{Style.BRIGHT}{Fore.MAGENTA}Zone de localisation: {Style.RESET_ALL}{', '.join(self.location_area)}{Style.RESET_ALL}\n"
        )

        def __eq__(self, other) -> bool:
            return (
                self.adref == other.adref
                and self.category_label == other.category_label
                and self.category_tag == other.category_tag
                and self.company_display_name == other.company_display_name
                and self.contract_time == other.contract_time
                and self.created == other.created
                and self.description == other.description
                and self.id == other.id
                and self.latitude == other.latitude
                and self.location_area == other.location_area
                and self.location_display_name == other.location_display_name
                and self.longitude == other.longitude
                and self.redirect_url == other.redirect_url
                and self.salary_is_predicted == other.salary_is_predicted
                and self.title == other.title
                and self.contract_type == other.contract_type
                and self.salary_max == other.salary_max
                and self.salary_min == other.salary_min
            )
