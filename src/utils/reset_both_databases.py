from utils.reset_database import ResetDatabase
from dao.db_connection import DBConnection

if __name__ == "__main__":
    # Reset prod database
    ResetDatabase().lancer()

    # Reset tests database
    DBConnection.CONNECTION_TO_TEST_DB = True
    ResetDatabase().lancer()

    DBConnection.CONNECTION_TO_TEST_DB = False
