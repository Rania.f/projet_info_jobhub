from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
from service.utilisateur_service import UtilisateurService


class ConnexionVue(VueAbstraite):
    def __init__(self, message=""):
        super().__init__(message)
        self.questions = [
            {"type": "input", "name": "pseudo", "message": "Entrez votre pseudo :"},
            {"type": "password", "name": "mdp", "message": "Entrez votre mot de passe :"},
        ]

    def choisir_menu(self):
        answers = prompt(self.questions)
        pseudo = answers["pseudo"]
        mdp = answers["mdp"]

        message = UtilisateurService().se_connecter(answers["pseudo"], answers["mdp"])

        print(message)

        if message == 'Vous êtes connecté !':
            from service.utilisateur_connecte import UtilisateurConnecte
            from dao.utilisateur_dao import UtilisateurDAO
            #utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo)
            #print(UtilisateurConnecte(pseudo= utilisateur.pseudo,mdp_hashe= utilisateur.mdp_hashe,nom= utilisateur.nom,preferences= utilisateur.preferences,adresse= utilisateur.adresse,prenom= utilisateur.prenom,mail= utilisateur.mail, tel=utilisateur.tel).alerte())


            from view.menu_utilisateur_vue import MenuUtilisateurVue
            return (MenuUtilisateurVue().choisir_menu(pseudo)) 

        else:
            from view.accueil_vue import AccueilVue
            return (AccueilVue().choisir_menu())

if __name__ == "__main__":
    vue = ConnexionVue()
    vue.choisir_menu()
