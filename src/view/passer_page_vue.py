from view.vue_abstraite import VueAbstraite
from service.utilisateur_recherche import RechercheService
from InquirerPy import prompt

class PasserPageVue(VueAbstraite):

    def __init__(self, message=""):
        super().__init__(message)
        self.questions = [
            {
                "type": "list",
                "name": "new_page",
                "message": "Souhaitez vous passer à la page suivante ?",
                "choices": [
                    "OUI",
                    "NON",
                ],
            }
        ]

    def choisir_menu(self, resultat, recherche, pseudo, liste_annonces):

        reponse = prompt(self.questions)

        new_page = reponse["new_page"] 

        if new_page == "OUI":
            stop = 0
            resultat = (RechercheService().passer_page_suivante(recherche))
            if resultat != 1:
                for i in resultat.listeAnnonces:
                    liste_annonces.append(i)
                print(resultat)
                return(stop, liste_annonces)
        
        if (new_page == "NON") :
            stop = 1 
            if pseudo == "":
                from view.accueil_vue import AccueilVue
                return(stop, liste_annonces)
            else: 
                from view.enregistrer_annonce_vue import EnregistrerAnnonceVue
                return(stop, EnregistrerAnnonceVue().choisir_menu(liste_annonces, pseudo))


if __name__ == "__main__":
    vue = PasserPageVue()
    vue.choisir_menu()