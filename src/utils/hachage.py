import hashlib
class Hachage:

    def sel_hachage(self, pseudo, mdp):
        mdp_sel = mdp + pseudo
        # On choisi un algorithme de hachage, par exemple SHA-256
        hash_algorithm = hashlib.sha256()   
        # On encode le mot de passe en bytes (UTF-8 est couramment utilisé)
        password_bytes = mdp_sel.encode('utf-8')
        # Mettez à jour l'objet de hachage avec les bytes du mot de passe
        hash_algorithm.update(password_bytes)
        # Obtenez le hachage sous forme de chaîne hexadécimale
        hashed_password = hash_algorithm.hexdigest()
        return(hashed_password)
    
if __name__ == "__main__":
    print(Hachage().sel_hachage('jeanmi','jeanmi'))
    print(Hachage().sel_hachage('lara','lara'))