import unittest
from utils.reset_database import ResetDatabase
from dao.annonce_dao import AnnonceDAO
from dao.db_connection import DBConnection
from business_object.annonce import Annonce
import psycopg2.errors


class AnnonceDaoTest(unittest.TestCase):
    annonce1 = Annonce(
        {
            "__CLASS__": "Adzuna::API::Response::Job",
            "adref": "eyJhbGciOiJIUzI1NiJ9.eyJpIjoiNDI4NDQ2ODY4MCIsInMiOiJkZ29QMWNxSzdoR0NyQzFpQWtoTDd3In0.O4vW-uQuST8ESBJzOXlpuD714sOp58CTqUKrSi9OI-8",
            "category": {
                "__CLASS__": "Adzuna::API::Response::Category",
                "label": "Emplois Autres/Général",
                "tag": "other-general-jobs",
            },
            "company": {
                "__CLASS__": "Adzuna::API::Response::Company",
                "display_name": "WAVY SERVICES",
            },
            "created": "2023-08-30T08:34:38Z",
            "description": "Nous recrutons un spécialiste Flux Data H/F basé à Rennes. Au sein du pôle DATA dans une DSI à taille humaine (environ 30 personnes), vous interviendrez, dans un premier temps, sur la rationalisation des flux existants et leur modernisation. Missions principales - Gérer les sujets d'interfaçages inter-applicatifs du groupe - Etre l'interlocuteur des métiers pour cadrer les besoins et les traduire techniquement - Piloter un portefeuille de projets - Organiser le travail à l'aide des ressources i…",
            "id": "4284468680",
            "latitude": 48.11176,
            "location": {
                "__CLASS__": "Adzuna::API::Response::Location",
                "area": ["France", "Bretagne", "Ille-et-Vilaine", "Rennes", "Betton"],
                "display_name": "Betton, Rennes",
            },
            "longitude": -1.68027,
            "redirect_url": "https://www.adzuna.fr/land/ad/4284468680?se=dgoP1cqK7hGCrC1iAkhL7w&utm_medium=api&utm_source=4aae40b4&v=BB8B9138017DC1EA074E575CA6B6A992AA553AF3",
            "salary_is_predicted": "0",
            "salary_max": 53000,
            "salary_min": 48000,
            "title": "Spécialiste Flux Data H/F",
        }
    )

    annonce2 = Annonce(
        {
            "__CLASS__": "Adzuna::API::Response::Job",
            "adref": "eyJhbGciOiJIUzI1NiJ9.eyJzIjoiZGdvUDFjcUs3aEdDckMxaUFraEw3dyIsImkiOiI0NDQ0MzY1OTY1In0.CUbBDT8MtY-FVkO4IUfWdJ0ajyxZCMuJkfHUzqJUY_s",
            "category": {
                "__CLASS__": "Adzuna::API::Response::Category",
                "label": "Emplois Comptabilité et Finance",
                "tag": "accounting-finance-jobs",
            },
            "company": {
                "__CLASS__": "Adzuna::API::Response::Company",
                "display_name": "ELSYS Design",
            },
            "created": "2023-11-21T06:53:14Z",
            "description": "Contexte et lieu de mission : ELSYS Design recherche un ingénieur Data Analyst dans le cadre d'une collaboration avec un de nos clients stratégique du secteur Ferroviaire. Dans ce contexte, vous serez amené à intégrer son équipe Data à Tours et participerez à un projet d'envergure sur la maintenance (Corrective/Prédictive) par l'analyse et l'interprétation des données issues des outils communicants. Responsabilités : - Analyse et interprétation de données - Écriture d'équations de code - Suppor…",
            "id": "4444365965",
            "latitude": 48.11842,
            "location": {
                "__CLASS__": "Adzuna::API::Response::Location",
                "area": [
                    "France",
                    "Bretagne",
                    "Ille-et-Vilaine",
                    "Rennes",
                    "Cesson-Sévigné",
                ],
                "display_name": "Cesson-Sévigné, Rennes",
            },
            "longitude": -1.60461,
            "redirect_url": "https://www.adzuna.fr/land/ad/4444365965?se=dgoP1cqK7hGCrC1iAkhL7w&utm_medium=api&utm_source=4aae40b4&v=61F687DA20FB2FD770AB4EF3705635D418ED2C68",
            "salary_is_predicted": "0",
            "salary_max": 55000,
            "salary_min": 35000,
            "title": "Ingénieur Data Analyse H/F",
        }
    )

    # annonce non présente dans la base de tests :
    annonce0 = Annonce(
        {
            "adref": "12345",
            "category": {"label": "Category", "tag": "Tag"},
            "company": {"display_name": "Company Inc."},
            "contract_time": "Full-time",
            "created": "2023-10-25 12:00:00",
            "description": "Job description",
            "id": "POUET POUET",
            "latitude": 40.7128,
            "location": {"area": ["Area 1", "Area 2"], "display_name": "Location"},
            "longitude": -74.0060,
            "redirect_url": "https://example.com/job",
            "salary_is_predicted": 1,
            "title": "Job Title",
            "contract_type": "Permanent",
            "salary_min": 30000,
        }
    )

    def setUp(cls):
        DBConnection.CONNECTION_TO_TEST_DB = True
        ResetDatabase().lancer()

    def tearDown(cls):
        DBConnection.CONNECTION_TO_TEST_DB = False

    def test_annonce_existe(self):
        # GIVEN
        # WHEN
        oui = AnnonceDAO().annonce_existe(AnnonceDaoTest().annonce1)
        non = AnnonceDAO().annonce_existe(AnnonceDaoTest().annonce0)
        # THEN
        self.assertTrue(oui)
        self.assertFalse(non)

    def test_creation_annonce(self):
        # GIVEN
        # WHEN
        success = AnnonceDAO().creation_annonce(AnnonceDaoTest().annonce0)
        oui = AnnonceDAO().annonce_existe(AnnonceDaoTest().annonce0)
        # THEN
        self.assertTrue(success)
        self.assertTrue(oui)
        # On teste qu'une tentative de créer une deuxième Raymonde provoque une erreur
        with self.assertRaises(psycopg2.errors.UniqueViolation):
            AnnonceDAO().creation_annonce(AnnonceDaoTest().annonce1)

    def test_supprime_annonce(self):
        # GIVEN
        # WHEN
        success = AnnonceDAO().supprime_annonce(AnnonceDaoTest().annonce2)
        non = AnnonceDAO().annonce_existe(AnnonceDaoTest().annonce2)
        # THEN
        self.assertTrue(success)
        self.assertFalse(non)
        # annonce1 est liée à une candidature, on ne peut pas la supprimer :
        with self.assertRaises(psycopg2.errors.ForeignKeyViolation):
            AnnonceDAO().supprime_annonce(AnnonceDaoTest().annonce1)

    def test_trouver_annonce_par_id(self):
        # GIVEN
        # WHEN
        annonce1_dao = AnnonceDAO().trouver_annonce_par_id(AnnonceDaoTest().annonce1.id)
        annonce0_dao = AnnonceDAO().trouver_annonce_par_id(AnnonceDaoTest().annonce0.id)
        # THEN
        self.assertEqual(AnnonceDaoTest().annonce1.id, annonce1_dao.id)
        self.assertIsNone(annonce0_dao)


if __name__ == "__main__":
    unittest.main()
