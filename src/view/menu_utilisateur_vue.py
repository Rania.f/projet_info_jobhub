from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
#from view.faire_recherche_vue import FaireRechercheVue
from view.modifier_profil_vue import ModifierProfilVue
from service.utilisateur_connecte import UtilisateurConnecte
import emoji


class MenuUtilisateurVue(VueAbstraite):
    """Vue du menu du joueur

    Attributes
    ----------
    message=''
        str

    Returns
    ------
    view
        retourne la prochaine vue, celle qui est choisie par l'utilisateur de l'application
    """

    def __init__(self, message="") -> None:
        super().__init__(message)
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Faites votre choix",
                "choices": [
                    "\U0001f50d Faire une recherche",
                    "\U0001f3e0 Voir son profil",
                    "\U0001f512 Se déconnecter",
                ],
            }
        ]

    def choisir_menu(self, pseudo):
        """Choix du menu suivant de l'utilisateur

        Return
        ------
        vue
            Retourne la vue choisie par l'utilisateur dans le terminal
        """
        reponse = prompt(self.questions)

        if reponse["choix"] == "\U0001f512 Se déconnecter":
            from view.accueil_vue import AccueilVue
            return AccueilVue().choisir_menu()
            
        elif reponse["choix"] == "\U0001f50d Faire une recherche":
            from view.faire_recherche_vue import FaireRechercheVue
            return FaireRechercheVue().choisir_menu(pseudo)

        elif reponse["choix"] == "\U0001f3e0 Voir son profil":
            from view.voir_profil_vue import VoirProfilVue
            from service.utilisateur_connecte import UtilisateurConnecte
            from dao.utilisateur_dao import UtilisateurDAO
            utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo)
            print(UtilisateurConnecte(
                utilisateur.pseudo, 
                utilisateur.mdp_hashe,
                utilisateur.nom,
                utilisateur.preferences,
                utilisateur.adresse,
                utilisateur.prenom,
                utilisateur.mail,
                utilisateur.tel,).voir_profil())
            return (VoirProfilVue().choisir_menu(pseudo))


if __name__ == "__main__":
    vue = MenuUtilisateurVue()
    vue.choisir_menu()
