import dotenv

from view.accueil_vue import AccueilVue

if __name__ == "__main__":
    # On charge les variables d'envionnement
    dotenv.load_dotenv(override=True)

    nb_erreurs = 0
    vue_courante = AccueilVue("Bienvenue")

    try:
        # Affichage des choix possibles
        AccueilVue("").afficher()
        vue_courante = AccueilVue("Bienvenue").choisir_menu()

    except Exception as e:
        print(e)
        nb_erreurs += 1
        vue_courante = AccueilVue(
            "Une erreur est survenue, retour au menu principal"
        ).choisir_menu()

    # Lorsque l on quitte l application
    print("---------------------------------")
    # print("Au revoir")
    
    # Affichage lapin !
    endscreen_file = open("data/endscreen.txt", "r", encoding="utf-8")
    endscreen = endscreen_file.read()
    print(endscreen)

    

    
    