class Candidature:
    def __init__(
        self, id_utilisateur: str, id_annonce: str, cv="", lettre_motivation=""
    ) -> None:
        self.cv = cv
        self.lettre_motivation = lettre_motivation
        self.id_utilisateur = id_utilisateur
        self.id_annonce = id_annonce

    def __str__(self):
        return f"ID utilisateur : {self.id_utilisateur}\nID annonce : {self.id_annonce}\nCV : {self.cv}\nLettre de motivation : {self.lettre_motivation}"
