from typing import List
from utils.singleton import Singleton
from business_object.recherche import Recherche
from business_object.pays_enum import PaysEnum

from dao.db_connection import DBConnection

import json


class RechercheDAO(metaclass=Singleton):
    def recherche_existe(self, nom_recherche: str, id_utilisateur: int) -> bool:
        """renvoie true si la recherche existe dans la base pour l'uilisateur,
        false sinon"""

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT EXISTS(                                 "
                    "  SELECT 1                                     "
                    "  FROM jobhub.recherche                        "
                    "  WHERE nom_recherche = %(nom_recherche)s and  "
                    "        id_utilisateur = %(id_utilisateur)s);  ",
                    {"nom_recherche": nom_recherche, "id_utilisateur": id_utilisateur},
                )

                res = cursor.fetchone()

        return res["exists"]

    def creation_recherche(self, recherche: Recherche, id_utilisateur: int) -> bool:
        """Ajoute une nouvelle recherche dans la table recherche"""
        created = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO jobhub.recherche (id_utilisateur, nom_recherche, "
                    "                              country, page, params)         "
                    "VALUES (%(id_utilisateur)s, %(nom_recherche)s, %(country)s,  "
                    "        %(page)s, %(params)s)                                "
                    "RETURNING id_recherche;                                      ",
                    {
                        "id_utilisateur": id_utilisateur,
                        "nom_recherche": recherche.nom,
                        "country": recherche.country.value,
                        "page": recherche.page,
                        "params": json.dumps(recherche.params),
                    },
                )
                res = cursor.fetchone()
        if res:
            created = True
            recherche.id_recherche = res["id_recherche"]

        return created

    def trouver_recherche_par_nom_et_utilisateur(
        self, nom: str, id_utilisateur: int
    ) -> Recherche:
        """renvoie la recherche de l'utilisateur d'identifiant id_utilisateur ayant
        nom pour nom de la recherche"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                   "
                        "  FROM jobhub.recherche                    "
                        "  WHERE nom_recherche = %(nom)s  and       "
                        "        id_utilisateur = %(id_utilisateur)s",
                        {
                            "nom": nom,
                            "id_utilisateur": id_utilisateur,
                        },
                    )
                    res = cursor.fetchone()
        except Exception as e:
            print(e)
            raise

        recherche = None
        if res:
            recherche = Recherche(
                nom=res["nom_recherche"],
                country=PaysEnum(res["country"]),
                page=res["page"],
                params=json.loads(res["params"]),
            )
            recherche.id_recherche = res["id_recherche"]

        return recherche

    def trouver_recherche_par_id_recherche(self, id_recherche: int) -> Recherche:
        """renvoie la recherche d'identifiant id_recherche"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                  "
                        "  FROM jobhub.recherche                "
                        "  WHERE id_recherche = %(id_recherche)s ",
                        {
                            "id_recherche": id_recherche,
                        },
                    )
                    res = cursor.fetchone()
        except Exception as e:
            print(e)
            raise

        recherche = None
        if res:
            recherche = Recherche(
                nom=res["nom_recherche"],
                country=PaysEnum(res["country"]),
                page=res["page"],
                params=json.loads(res["params"]),
            )
            recherche.id_recherche = res["id_recherche"]

        return recherche

    def trouver_recherche_par_nom_recherche_et_id_utilisateur(
        self, nom_recherche: str, id_utilisateur: int
    ) -> Recherche:
        """renvoie la recherche de nom nom__recherche de l'utilisateur
        d'identifiant id_utilisateur"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                      "
                        "  FROM jobhub.recherche                       "
                        "  WHERE nom_recherche = %(nom_recherche)s and "
                        "        id_utilisateur = %(id_utilisateur)s   ",
                        {
                            "nom_recherche": nom_recherche,
                            "id_utilisateur": id_utilisateur,
                        },
                    )
                    res = cursor.fetchone()
        except Exception as e:
            print(e)
            raise

        recherche = None
        if res:
            recherche = Recherche(
                nom=res["nom_recherche"],
                country=PaysEnum(res["country"]),
                page=res["page"],
                params=json.loads(res["params"]),
            )
            recherche.id_recherche = res["id_recherche"]

        return recherche

    def supprime_recherche(self, recherche: Recherche) -> bool:
        """supprime une recherche de la table recherche"""
        deleted = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "delete from jobhub.recherche                     "
                    " WHERE id_recherche = %(id_recherche)s",
                    {
                        "id_recherche": recherche.id_recherche,
                    },
                )
                if cursor.rowcount:
                    deleted = True
        return deleted

    def trouver_noms_recherche_pour_id_utilisateur(
        self, id_utilisateur: int
    ) -> List[str]:
        """Renvoie une liste des noms de toutes les recherches d'un utilisateur"""
        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT nom_recherche                                   "
                    "  FROM jobhub.recherche                    "
                    "  WHERE id_utilisateur = %(id_utilisateur)s",
                    {"id_utilisateur": id_utilisateur},
                )
                res = cursor.fetchall()

        noms_recherche: List[str] = []

        if res:
            for row in res:
                noms_recherche.append(row["nom_recherche"])

        return noms_recherche

    def trouver_toutes_recherches_pour_id_utilisateur(
        self, id_utilisateur: int
    ) -> List[Recherche]:
        """Renvoie une liste de toutes les recherches d'un utilisateur
        (C'est une liste d'objets de la classe Recherche)"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                    "
                        "   FROM jobhub.recherche                    "
                        "   WHERE id_utilisateur = %(id_utilisateur)s",
                        {"id_utilisateur": id_utilisateur},
                    )
                    res = cursor.fetchall()
        except Exception as e:
            print(e)
            raise

        liste_recherches = []

        if res:
            for row in res:
                recherche = Recherche(
                    nom=row["nom_recherche"],
                    country=PaysEnum(row["country"]),
                    page=row["page"],
                    params=json.loads(row["params"]),
                )

                recherche.id_recherche = row["id_recherche"]

                liste_recherches.append(recherche)

        return liste_recherches
