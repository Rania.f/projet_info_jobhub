from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
from service.utilisateur_connecte import UtilisateurConnecte


class ModifPrefVue(VueAbstraite):
    def __init__(self):
        message = "Modifier ses préférences"
        super().__init__(message)
        self.questions = [
            {
                "type": "input",
                "name": "metier",
                "message": "Entrez le métier que vous cherchez :",
            },
            {
                "type": "input",
                "name": "localisation",
                "message": "Entrez la localisation que vous souhaitez :",
            },
            {
                "type": "input",
                "name": "salaire_min",
                "message": "Entrez le salaire minimum que vous voulez :",
            },
            
            {
                "type": "list",
                "name": "contrat",
                "message": "Quel type de contrat souhaitez vous ?",
                "choices": [
                    "CDI",
                    "CDD",
                    "Indifférent",
                ],
            },
        ]

    def choisir_menu(self, pseudo):
        from view.menu_utilisateur_vue import MenuUtilisateurVue
        from dao.utilisateur_dao import UtilisateurDAO

        utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo)

        answers = prompt(self.questions)
        metier = answers["metier"]
        contrat = answers["contrat"]
        localisation = answers["localisation"]
        salaire_min = answers["salaire_min"]

        print(UtilisateurConnecte(
                utilisateur.pseudo,
                utilisateur.mdp_hashe,
                utilisateur.nom,
                utilisateur.preferences,
                utilisateur.adresse,
                utilisateur.prenom,
                utilisateur.mail,
                utilisateur.tel,
            ).modifier_preferences(
                metier=metier,
                contrat=contrat,
                localisation=localisation,
                salaire_min=salaire_min,
            ))
        return(
            MenuUtilisateurVue().choisir_menu(pseudo)
        )


if __name__ == "__main__":
    vue = ModifPrefVue()
    vue.choisir_menu()
