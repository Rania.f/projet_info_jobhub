from business_object.pays_enum import PaysEnum


class Recherche:
    def __init__(self, nom: str, country: PaysEnum, page: int, params={}):
        self.id_recherche = 0
        self.nom = nom
        self.country = country
        self.page = page
        self.params = params

    def __str__(self):
        f"Identifiant : {self.id_recherche}, Nom : {self.nom}, Country : {self.country}, Page : {self.page}, Params : {self.params}"
