from typing import List
from utils.singleton import Singleton
from business_object.candidature import Candidature

from dao.db_connection import DBConnection


class CandidatureDAO(metaclass=Singleton):
    def candidature_existe(self, id_utilisateur, id_annonce) -> bool:
        """renvoie true si la candidature existe dans la base,
        false sinon"""

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT EXISTS(                             "
                    "  SELECT 1                                 "
                    "  FROM jobhub.candidature                  "
                    "  WHERE id_utilisateur = %(id_utilisateur)s"
                    "        and id_annonce = %(id_annonce)s  );",
                    {
                        "id_utilisateur": id_utilisateur,
                        "id_annonce": id_annonce,
                    },
                )

                res = cursor.fetchone()

        return res["exists"]

    def trouver_candidature_par_id_utilisateur_et_id_annonce(
        self, id_utilisateur: int, id_annonce: str
    ) -> Candidature:
        """renvoie la candidature de l'utilisateur d'identifiant id_utilisateur"""
        """ pour l'annonce d'identifiant id_annonce."""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                         "
                        "  FROM jobhub.candidature                        "
                        "  WHERE id_utilisateur = %(id_utilisateur)s and  "
                        "        id_annonce = %(id_annonce)s  ;           ",
                        {
                            "id_utilisateur": id_utilisateur,
                            "id_annonce": id_annonce,
                        },
                    )
                    res = cursor.fetchone()
        except Exception as e:
            print(e)
            raise

        candidature = None
        if res:
            candidature = Candidature(
                id_utilisateur=res["id_utilisateur"],
                id_annonce=res["id_annonce"],
                cv=res["cv"],
                lettre_motivation=res["lettre_motivation"],
            )

        return candidature

    ###

    def trouver_toutes_candidatures_pour_id_utilisateur(
        self, id_utilisateur: int
    ) -> List[Candidature]:
        """Renvoie une liste de toutes les candidatures d'un utilisateur
        (C'est une liste d'objets de la classe Candidature)"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                    "
                        "   FROM jobhub.candidature                    "
                        "   WHERE id_utilisateur = %(id_utilisateur)s",
                        {"id_utilisateur": id_utilisateur},
                    )
                    res = cursor.fetchall()
        except Exception as e:
            print(e)
            raise

        liste_candidatures = []

        if res:
            for row in res:
                candidature = Candidature(
                    id_utilisateur=row["id_utilisateur"],
                    id_annonce=row["id_annonce"],
                    cv=row["cv"],
                    lettre_motivation=row["lettre_motivation"],
                )

                liste_candidatures.append(candidature)

        return liste_candidatures

    ###

    def creation_candidature(self, candidature: Candidature) -> bool:
        """Ajoute une nouvelle candidature dans la table candidature"""
        created = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO jobhub.candidature (id_utilisateur, id_annonce, cv, "
                    "                                lettre_motivation)              "
                    "VALUES (%(id_utilisateur)s, %(id_annonce)s, %(cv)s,             "
                    "        %(lettre_motivation)s)                                  "
                    "RETURNING id_candidature;                                       ",
                    {
                        "id_utilisateur": candidature.id_utilisateur,
                        "id_annonce": candidature.id_annonce,
                        "cv": candidature.cv,
                        "lettre_motivation": candidature.lettre_motivation,
                    },
                )
                res = cursor.fetchone()
        if res:
            created = True

        return created

    def miseajour_candidature(self, candidature: Candidature) -> bool:
        """Met à jour une candidature déjà créée"""
        updated = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "UPDATE jobhub.candidature                            "
                    "   SET cv = %(cv)s,                                  "
                    "       lettre_motivation = %(lettre_motivation)s     "
                    " WHERE id_utilisateur = %(id_utilisateur)s and       "
                    "       id_annonce = %(id_annonce)s                   ",
                    {
                        "cv": candidature.cv,
                        "lettre_motivation": candidature.lettre_motivation,
                        "id_utilisateur": candidature.id_utilisateur,
                        "id_annonce": candidature.id_annonce,
                    },
                )
                if cursor.rowcount:
                    updated = True
        return updated

    def supprime_candidature(self, candidature: Candidature) -> bool:
        """supprime une candidature, renvoie true si réussite, false sinon"""
        deleted = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "delete from jobhub.candidature                     "
                    " WHERE id_utilisateur = %(id_utilisateur)s and     "
                    "       id_annonce = %(id_annonce)s                 ",
                    {
                        "id_utilisateur": candidature.id_utilisateur,
                        "id_annonce": candidature.id_annonce,
                    },
                )
                if cursor.rowcount:
                    deleted = True
        return deleted
