from business_object.annonce import Annonce
from business_object.recherche import Recherche
from colorama import Fore, Style


class ResultatRecherche:
    def __init__(
        self,
        recherche: Recherche,
        nombreAnnonces: int,
        salaireMoyen: float,
        listeAnnonces: list[Annonce],
    ):  # Il s'agit d'une liste d'annonces
        self.nombreAnnonces = nombreAnnonces
        self.salaireMoyen = salaireMoyen
        self.listeAnnonces = listeAnnonces
        self.recherche = recherche

    def __str__(self):
        """
        Afficher le résultat d'une recherche

        Parameters
        ----------
        recherche(Recherche)
            recherche réalisée
        resultat_recherche(ResultatRecherche)
            Résultat d'une recherche
        """
        separator_line = f"{Fore.WHITE}{'-' * 50}{Style.RESET_ALL}\n"
        output = (
            f"{Style.BRIGHT}{Fore.CYAN}Résultat de la recherche {Style.RESET_ALL}\n\n"
        )
        output += f"{Style.BRIGHT}{Fore.YELLOW}Nombre d'annonces: {Style.RESET_ALL}{Style.BRIGHT}{self.nombreAnnonces} {Style.RESET_ALL} \n"
        output += f"{Style.BRIGHT}{Fore.YELLOW}Salaire moyen: {Style.RESET_ALL}{Style.BRIGHT}{self.salaireMoyen}{Style.RESET_ALL} \n"

        if self.listeAnnonces != []:
            output += f"\n{Style.BRIGHT}{Fore.CYAN}Voici les annonces correspondantes:{Style.RESET_ALL}\n\n"
            output += f" \n{separator_line}\n"

            for annonce in self.listeAnnonces:
                output += str(annonce) + f" \n{separator_line}\n"

        else:
            output += f"{Fore.RED}Aucune annonce trouvée.{Style.RESET_ALL}\n"

        return output
