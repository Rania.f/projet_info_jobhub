--
-- Jobhub initialisation des bases de données relationnelles
--

drop schema if exists jobhub cascade;
create schema jobhub;

-- Utilisateur
create table jobhub.utilisateur (
	id_utilisateur  serial primary key,
	pseudo text unique,
    mdp_hashe text,
    nom text,
    prenom text,
    adresse text,
    mail text,
    tel text,
    preferences text
);

-- Annonce
create table jobhub.annonce(
    id_annonce text primary key,
    adref TEXT,
    category_label TEXT,
    category_tag TEXT,
    company_display_name TEXT,
    contract_time TEXT,
    created TIMESTAMP,
    description TEXT,
    latitude NUMERIC,
    location_area TEXT[],
    location_display_name TEXT,
    longitude NUMERIC,
    redirect_url TEXT,
    salary_is_predicted INT,
    title TEXT,
    contract_type TEXT,
    salary_max NUMERIC,
    salary_min numeric);

-- Candidature
create table jobhub.candidature(
    id_candidature serial primary key,
    cv text,
    lettre_motivation text,
    id_utilisateur integer references jobhub.utilisateur on delete cascade,
    id_annonce text references jobhub.annonce,
    constraint candidature_unique unique(id_utilisateur, id_annonce)

);

-- Recherche
create table jobhub.recherche(
    id_recherche serial primary key,
    id_utilisateur int references jobhub.utilisateur on delete cascade,
    nom_recherche text,
    country text not null,
    page integer not null,
    params text,
    constraint nom_recherche_unique unique(nom_recherche, id_utilisateur),
    constraint country check (country IN ('fr', 'gb', 'us', 'at', 'au', 'be', 'br', 'ca', 'ch', 'de', 'es', 'in', 'it', 'mx', 'nl', 'nz', 'pl', 'ru', 'sg', 'za'))
);

-- Résultat recherche
create table jobhub.resultat_recherche(
    id_resultat serial primary key,
    id_recherche int references jobhub.recherche on delete cascade,
    nombre_annonces integer,
    salaire_moyen float,
    liste_id_annonces text[] 

)