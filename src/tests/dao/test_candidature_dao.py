import unittest
from utils.reset_database import ResetDatabase
from dao.candidature_dao import CandidatureDAO
from dao.db_connection import DBConnection
from business_object.candidature import Candidature
import psycopg2.errors


class CandidatureDaoTest(unittest.TestCase):
    def setUp(cls):
        DBConnection.CONNECTION_TO_TEST_DB = True
        ResetDatabase().lancer()

    def tearDown(cls):
        DBConnection.CONNECTION_TO_TEST_DB = False

    def test_candidature_existe(self):
        # GIVEN
        id_utilisateur_jeanmi = 1
        id_annonce1 = "4284468680"
        id_annonce2 = "4444365965"
        # WHEN
        oui = CandidatureDAO().candidature_existe(id_utilisateur_jeanmi, id_annonce1)
        non = CandidatureDAO().candidature_existe(id_utilisateur_jeanmi, id_annonce2)
        # THEN
        self.assertTrue(oui)
        self.assertFalse(non)

    def test_trouver_candidature_par_id_utilisateur_et_id_annonce(self):
        # GIVEN
        id_utilisateur_jeanmi = 1
        id_annonce1 = "4284468680"
        id_annonce2 = "4444365965"
        # WHEN
        # candidature1_dao existe mais pas candidature2_dao :
        candidature1_dao = (
            CandidatureDAO().trouver_candidature_par_id_utilisateur_et_id_annonce(
                id_utilisateur_jeanmi, id_annonce1
            )
        )
        candidature2_dao = (
            CandidatureDAO().trouver_candidature_par_id_utilisateur_et_id_annonce(
                id_utilisateur_jeanmi, id_annonce2
            )
        )
        # THEN
        self.assertEqual(id_utilisateur_jeanmi, candidature1_dao.id_utilisateur)
        self.assertEqual(id_annonce1, candidature1_dao.id_annonce)
        self.assertIsNone(candidature2_dao)

    def test_trouver_toutes_candidatures_pour_id_utilisateur(self):
        # GIVEN
        # WHEN
        liste1_candidatures = (
            CandidatureDAO().trouver_toutes_candidatures_pour_id_utilisateur(1)
        )
        liste2_candidatures = (
            CandidatureDAO().trouver_toutes_candidatures_pour_id_utilisateur(2)
        )
        # THEN
        self.assertEqual(1, len(liste1_candidatures))
        self.assertEqual(0, len(liste2_candidatures))

    def test_creation_candidature(self):
        # GIVEN
        # candidature1 existe déjà, candidature2 n'existe pas encore :
        candidature1 = Candidature(1, "4284468680")
        candidature2 = Candidature(1, "4444365965")
        # WHEN
        oui = CandidatureDAO().creation_candidature(candidature2)
        # THEN
        self.assertTrue(oui)
        # candidature1 existe déjà, erreur ! :
        with self.assertRaises(psycopg2.errors.UniqueViolation):
            CandidatureDAO().creation_candidature(candidature1)

    def test_miseajour_candidature(self):
        # GIVEN
        candidature1 = Candidature(
            1, "4284468680", "C:/moncv.pdf", "C:/lettremotiv.pdf"
        )
        # WHEN
        success = CandidatureDAO().miseajour_candidature(candidature1)
        candidature1_dao = (
            CandidatureDAO().trouver_candidature_par_id_utilisateur_et_id_annonce(
                1, "4284468680"
            )
        )
        # THEN
        self.assertTrue(success)
        self.assertEqual(candidature1.cv, candidature1_dao.cv)
        self.assertEqual(
            candidature1.lettre_motivation, candidature1_dao.lettre_motivation
        )

    def test_supprime_candidature(self):
        # GIVEN
        # candidature1 existe déjà, candidature2 n'existe pas encore :
        candidature1 = Candidature(1, "4284468680")
        candidature2 = Candidature(1, "4444365965")
        # WHEN
        success1 = CandidatureDAO().supprime_candidature(candidature1)
        non = CandidatureDAO().candidature_existe(
            candidature1.id_utilisateur, candidature1.id_annonce
        )
        success2 = CandidatureDAO().supprime_candidature(candidature2)

        # THEN
        self.assertTrue(success1)
        self.assertFalse(non)
        self.assertFalse(success2)


if __name__ == "__main__":
    unittest.main()
