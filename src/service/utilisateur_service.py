from dao.utilisateur_dao import UtilisateurDAO
from business_object.utilisateur import Utilisateur
from utils.hachage import Hachage

## Faire se déconnecter

class UtilisateurService():

    def creer_compte(self, pseudo, mdp1, mdp2):

        """ Permet de se créer un compte si :
        - le pseudo qui n'est pas déja utilisé par un autre utilisateur, 
        - le mot de passe et le mot de passe de confirmation sont identiques.

        Parameters
        ----------
        pseudo
            Pseudonyme de l'utilisateur
        mdp1
            Mot de passe choisi pour sécuriser le compte
        mdp2
            Confirmation du mot de passe choisi

        Returns
        -------
        str 
            Confirmation que le compte est créé ou erreur à corriger

        """
        # si id n'est pas dans la base de donnée on peut garder cet identifiant
        # sinon on change
        # si mdp1 == mp2 on enregistre les infos dans la base de donnée

        if (UtilisateurDAO().pseudo_existe(pseudo)):
            return("Veuillez changer d'identifiant, un autre utilisateur l'utilise déja.")
        if (mdp1 != mdp2):
            return("Les deux mots de passe ne sont pas identiques.")
        mdp_hache = Hachage().sel_hachage(pseudo, mdp1)
        utilisateur = Utilisateur(pseudo = pseudo, mdp_hashe = mdp_hache)
        UtilisateurDAO().creation_utilisateur(utilisateur)
        return('Votre compte est créé !')

    def se_connecter(self, pseudo, mdp):

        """ Permet de se connecter à son compte. 

        Parameters
        ----------
        pseudo
            Pseudonyme de l'utilisateur
        mdp
            Mot de passe de vérification

        Returns
        -------
        str 
            Confirmation de la connection ou erreur à corriger

        """
        if (UtilisateurDAO().pseudo_existe(pseudo)):
            utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo)
            mdp_hache = Hachage().sel_hachage(pseudo, mdp)
            if (utilisateur.mdp_hashe == mdp_hache):
                return("Vous êtes connecté !")
            else:
                return("Vous vous êtes trompé de mot de passe.")
        return("Vous vous êtes trompé de nom d'utilisateur ou vous n'avez pas de compte.")



if __name__ == "__main__":
    print(UtilisateurService().creer_compte('id10', 'mdp10', 'mdp10'))
    print(UtilisateurService().creer_compte('id10', 'mdp10', 'mdp10'))
    print(UtilisateurService().creer_compte('id3', 'mdp3', 'mdp10'))
    print(UtilisateurService().se_connecter('id10', 'mdp10'))
    print(UtilisateurService().se_connecter('id10', 'mdp3'))
    print(UtilisateurService().se_connecter('id3', 'mdp3'))
    print(UtilisateurService().se_connecter('id10', "mdp10"))

