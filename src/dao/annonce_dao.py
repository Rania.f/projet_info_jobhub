from utils.singleton import Singleton
from business_object.annonce import Annonce

from dao.db_connection import DBConnection


class AnnonceDAO(metaclass=Singleton):
    def annonce_existe(self, annonce: Annonce) -> bool:
        """renvoie true si l'annonce existe dans la base,
        false sinon"""

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "SELECT EXISTS(                           "
                    "  SELECT 1                               "
                    "  FROM jobhub.annonce                "
                    "  WHERE id_annonce = %(id_annonce)s);           ",
                    {"id_annonce": annonce.id},
                )

                res = cursor.fetchone()

        return res["exists"]

    def creation_annonce(self, annonce: Annonce) -> bool:
        """Ajoute une nouvelle annonce dans la table annonce"""
        created = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO jobhub.annonce (adref,                          "
                    "                            category_label,                 "
                    "                            category_tag,                   "
                    "                            company_display_name,           "
                    "                            contract_time,                  "
                    "                            created,                        "
                    "                            description,                    "
                    "                            id_annonce,                     "
                    "                            latitude,                       "
                    "                            location_area,                  "
                    "                            location_display_name,          "
                    "                            longitude,                      "
                    "                            redirect_url,                   "
                    "                            salary_is_predicted,            "
                    "                            title,                          "
                    "                            contract_type,                  "
                    "                            salary_min,                     "
                    "                            salary_max)                     "
                    "VALUES (%(adref)s, %(category_label)s, %(category_tag)s,    "
                    "        %(company_display_name)s, %(contract_time)s,        "
                    "        %(created)s, %(description)s, %(id_annonce)s,       "
                    "        %(latitude)s, %(location_area)s,                    "
                    "        %(location_display_name)s, %(longitude)s,           "
                    "        %(redirect_url)s, %(salary_is_predicted)s,          "
                    "        %(title)s,                 "
                    "        %(contract_type)s, %(salary_min)s, %(salary_max)s   "
                    "        )                                                   "
                    "RETURNING id_annonce;",
                    {
                        "adref": annonce.adref,
                        "category_label": annonce.category_label,
                        "category_tag": annonce.category_tag,
                        "company_display_name": annonce.company_display_name,
                        "contract_time": annonce.contract_time,
                        "created": annonce.created,
                        "description": annonce.description,
                        "id_annonce": annonce.id,
                        "latitude": annonce.latitude,
                        "location_area": annonce.location_area,
                        "location_display_name": annonce.location_display_name,
                        "longitude": annonce.longitude,
                        "redirect_url": annonce.redirect_url,
                        "salary_is_predicted": annonce.salary_is_predicted,
                        "title": annonce.title,
                        "contract_type": annonce.contract_type,
                        "salary_min": annonce.salary_min,
                        "salary_max": annonce.salary_max,
                    },
                )
                res = cursor.fetchone()
        if res:
            created = True

        return created

    def supprime_annonce(self, annonce: Annonce) -> bool:
        """supprime une annonce de la table annonce"""
        deleted = False

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "delete from jobhub.annonce                     "
                    " WHERE id_annonce = %(id_annonce)s                      ",
                    {
                        "id_annonce": annonce.id,
                    },
                )
                if cursor.rowcount:
                    deleted = True
        return deleted

    def trouver_annonce_par_id(self, id_annonce: str) -> Annonce:
        """renvoie l'annonce ayant l'identifiant id_annonce"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                      "
                        "  FROM jobhub.annonce                         "
                        "  WHERE id_annonce = %(id_annonce)s           ",
                        {
                            "id_annonce": id_annonce,
                        },
                    )
                    res = cursor.fetchone()
        except Exception as e:
            print(e)
            raise

        annonce = None
        if res:
            annonce = Annonce(
                {
                    "adref": res["adref"],
                    # "category_label": res["category_label"],
                    # "category_tag": res["category_tag"],
                    "category": {
                        "label": res["category_label"],
                        "tag": res["category_tag"],
                    },
                    # "company_display_name": res["company_display_name"],
                    "company": {"display_name": res["company_display_name"]},
                    "contract_time": res["contract_time"],
                    "created": str(res["created"]),
                    "description": res["description"],
                    "id": res["id_annonce"],
                    "latitude": res["latitude"],
                    # "location_area": res["location_area"],
                    # "location_display_name": res["location_display_name"],
                    "location": {
                        "area": res["location_area"],
                        "display_name": res["location_display_name"],
                    },
                    "longitude": res["longitude"],
                    "redirect_url": res["redirect_url"],
                    "salary_is_predicted": res["salary_is_predicted"],
                    "title": res["title"],
                    "contract_type": res["contract_type"],
                    "salary_min": res["salary_min"],
                    "salary_max": res["salary_max"],
                }
            )

        return annonce
