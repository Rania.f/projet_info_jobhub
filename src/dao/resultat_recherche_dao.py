from utils.singleton import Singleton
from business_object.resultat_recherche import ResultatRecherche
from dao.annonce_dao import AnnonceDAO
from dao.recherche_dao import RechercheDAO

from dao.db_connection import DBConnection


class ResultatRechercheDAO(metaclass=Singleton):
    def creation_resultat_recherche(self, resultatRecherche: ResultatRecherche) -> bool:
        """Ajoute le résultat d'une recherche dans la table resultat_recherche"""
        created = False

        liste_id_annonces = []

        for annonce in resultatRecherche.listeAnnonces:
            liste_id_annonces.append(annonce.id)

        with DBConnection().connection as connection:
            with connection.cursor() as cursor:
                cursor.execute(
                    "INSERT INTO jobhub.resultat_recherche (id_recherche,             "
                    "      nombre_annonces, salaire_moyen, liste_id_annonces)         "
                    "VALUES (%(id_recherche)s, %(nombre_annonces)s, %(salaire_moyen)s,"
                    "        %(liste_id_annonces)s)                                   "
                    "RETURNING id_resultat;                                           ",
                    {
                        "id_recherche": resultatRecherche.recherche.id_recherche,
                        "nombre_annonces": resultatRecherche.nombreAnnonces,
                        "salaire_moyen": resultatRecherche.salaireMoyen,
                        "liste_id_annonces": liste_id_annonces,
                    },
                )
                res = cursor.fetchone()

        for annonce in resultatRecherche.listeAnnonces:
            if not AnnonceDAO().annonce_existe(annonce):
                resAnnonce = AnnonceDAO().creation_annonce(annonce)
                res = res and resAnnonce

        if res:
            created = True

        return created

    def trouver_resultat_recherche_par_id_recherche(
        self, id_recherche: int
    ) -> ResultatRecherche:
        """renvoie le résultat de la recherche d'identifiant id_recherche"""
        try:
            with DBConnection().connection as connection:
                with connection.cursor() as cursor:
                    cursor.execute(
                        "SELECT *                                  "
                        "  FROM jobhub.resultat_recherche          "
                        "  WHERE id_recherche = %(id_recherche)s   ",
                        {
                            "id_recherche": id_recherche,
                        },
                    )
                    res = cursor.fetchone()
        except Exception as e:
            print(e)
            raise

        resultat_recherche = None
        if res:
            liste_id_annonces = res["liste_id_annonces"]
            listeAnnonces = []
            for id_annonce in liste_id_annonces:
                listeAnnonces.append(AnnonceDAO().trouver_annonce_par_id(id_annonce))

            recherche = RechercheDAO().trouver_recherche_par_id_recherche(
                res["id_recherche"]
            )
            resultat_recherche = ResultatRecherche(
                recherche=recherche,
                nombreAnnonces=res["nombre_annonces"],
                salaireMoyen=res["salaire_moyen"],
                listeAnnonces=listeAnnonces,
            )

        return resultat_recherche
