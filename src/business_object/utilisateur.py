class Utilisateur:
    def __init__(
        self,
        pseudo: str,
        mdp_hashe: str,
        nom="",
        preferences={},
        adresse="",
        prenom="",
        mail="",
        tel="",
    ):
        self.id_utilisateur = 0
        self.pseudo = pseudo
        self.mdp_hashe = mdp_hashe
        self.nom = nom
        self.prenom = prenom
        self.preferences = preferences
        self.adresse = adresse
        self.mail = mail
        self.tel = tel

    def __str__(self):
        return f"Utilisateur - Identifiant: {self.id_utilisateur}, Pseudo: {self.pseudo}, Nom: {self.nom}, Prénom: {self.prenom}, Adresse: {self.adresse}, Préférences: {self.preferences}, Mot de Passe : {self.mdp_hashe}, Mail: {self.mail}, Numero de Téléphone :{self.tel} "
