from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
from service.utilisateur_connecte import UtilisateurConnecte


class ModifInfoVue(VueAbstraite):
    def __init__(self):
        message = "Modifier ses informations personnelles."
        super().__init__(message)
        self.questions = [
            {"type": "input", "name": "nom", "message": "Entrez votre nom :"},
            {"type": "input", "name": "prenom", "message": "Entrez votre prénom :"},
            {"type": "input", "name": "mail", "message": "Entrez votre adresse mail :"},
            {
                "type": "input",
                "name": "tel",
                "message": "Entrez votre numéro de téléphone :",
            },
            {
                "type": "input",
                "name": "adresse",
                "message": "Entrez votre adresse postale :",
            },
        ]

    def choisir_menu(self, pseudo):
        from view.menu_utilisateur_vue import MenuUtilisateurVue
        from dao.utilisateur_dao import UtilisateurDAO

        utilisateur = UtilisateurDAO().trouver_utilisateur_par_pseudo(pseudo)

        answers = prompt(self.questions)
        nom = answers["nom"]
        prenom = answers["prenom"]
        mail = answers["mail"]
        tel = answers["tel"]
        adresse = answers["adresse"]
        
        print(UtilisateurConnecte(
                utilisateur.pseudo,
                utilisateur.mdp_hashe,
                utilisateur.nom,
                utilisateur.preferences,
                utilisateur.adresse,
                utilisateur.prenom,
                utilisateur.mail,
                utilisateur.tel,
            ).modifier_infos_perso(
                nom=nom, prenom=prenom, mail=mail, tel=tel, adresse=adresse
            ))

        return(
            MenuUtilisateurVue().choisir_menu(pseudo)
        )


if __name__ == "__main__":
    vue = ModifInfoVue()
    vue.choisir_menu()
