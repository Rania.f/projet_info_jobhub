from InquirerPy import prompt
from view.vue_abstraite import VueAbstraite
from view.modif_pref_vue import ModifPrefVue
from view.modif_info_vue import ModifInfoVue
from view.modif_mdp_vue import ModifMdpVue
import emoji

class ModifierProfilVue(VueAbstraite):
    def __init__(self, message=""):
        super().__init__(message)
        self.questions = [
            {
                "type": "list",
                "name": "choix",
                "message": "Que voulez vous modifier ?",
                "choices": [
                    "\U0001f3e0 Modifier ses informations personnelles",
                    "\U0001f510 Modifier son mot de passe",
                    "\u2764\uFE0F  Modifier ses préférences",
                ],
            }
        ]

    def choisir_menu(self, pseudo):
        reponse = prompt(self.questions)
        if reponse["choix"] == "\U0001f3e0 Modifier ses informations personnelles":
            return ModifInfoVue().choisir_menu(pseudo)
        elif reponse["choix"] == "\U0001f510 Modifier son mot de passe":
            return ModifMdpVue().choisir_menu(pseudo)
        elif reponse["choix"] == "\u2764\uFE0F  Modifier ses préférences":
            return ModifPrefVue().choisir_menu(pseudo)


if __name__ == "__main__":
    vue = ModifierProfilVue()
    vue.choisir_menu()
