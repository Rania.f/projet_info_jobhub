import os
import dotenv
import psycopg2
from psycopg2.extras import RealDictCursor
from utils.singleton import Singleton


class DBConnection(metaclass=Singleton):
    """
    Technical class to open only one connection to the DB.
    """

    CONNECTION_TO_TEST_DB = False

    def __init__(self):
        dotenv.load_dotenv(override=True)

        # Open the connection to the test database...
        if DBConnection.CONNECTION_TO_TEST_DB:
            self.__connection = psycopg2.connect(
                host=os.environ["HOST"],
                port=os.environ["PORT"],
                database=os.environ["DATABASETEST"],
                user=os.environ["USERTEST"],
                password=os.environ["PASSWORDTEST"],
                cursor_factory=RealDictCursor,
            )
        # ...or to the prod database :
        else:
            self.__connection = psycopg2.connect(
                host=os.environ["HOST"],
                port=os.environ["PORT"],
                database=os.environ["DATABASE"],
                user=os.environ["USER"],
                password=os.environ["PASSWORD"],
                cursor_factory=RealDictCursor,
            )

    @property
    def connection(self):
        """
        :return: the opened connection.
        """
        return self.__connection
